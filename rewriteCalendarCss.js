/* eslint-disable */
const path = require('path');
const fs = require('fs');

function changePX2Rem(value) {
  return value.replace(/(\d+(\.\d+)?)px/ig, (match, p1, p2) => {
    const oldValue = Number(p1);
    let newValue;
    if (oldValue === 1) {
      newValue  = `${oldValue}px`;
    } else {
      newValue = `${(oldValue * 3)/100}rem`;
    }
    return newValue;
  });
}

function rewrite(filepath, newFile) {
  fs.stat(filepath, (error, stats) => {
    if (error) {
      console.error(error);
      return;
    }
    const content = fs.readFileSync(filepath, {
      encoding: 'utf8',
    });
    const newContent = changePX2Rem(content);
    fs.writeFileSync(newFile, newContent, {
      encoding: 'utf8',
    });
  });
}

const filepath = path.resolve('./node_modules/rc-calendar/assets', 'index.css');
const newFile = path.resolve('./public/', 'rc-calendar.css')

rewrite(filepath, newFile)
