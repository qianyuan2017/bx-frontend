import React from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Helmet } from 'react-helmet';
import { Flex, InputItem, WhiteSpace, Button } from 'antd-mobile';
import route from '../../utils/routes';

const LogIn = (props) => {
  let empid = '';

  const handleEmpIdChange = (val) => {
    empid = val;
  };

  const handleSubmit = () => {
    if (!empid || empid === '') {
      props.dispatch({ type: 'app/alert', payload: { title: '必填', message: '请填写您的工号!' } });
      return undefined;
    }

    props.dispatch({ type: 'app/updateUser', payload: { empid } });
    props.dispatch(routerRedux.push(route.mine.index));
  };

  return (
    <div>
      <Helmet>
        <title>身份确认</title>
      </Helmet>
      <div style={{ padding: '.256rem .32rem' }}>
        <p style={{ textAlign: 'center', fontSize: '1.5em' }}>身份确认</p>
        <Flex justify="center" align="center">
          <Flex.Item>
            <InputItem onChange={handleEmpIdChange} placeholder="请输入您的工号" />
          </Flex.Item>
        </Flex>
        <WhiteSpace size="lg" />
        <div style={{ padding: '.32rem 0' }}>
          <Button onClick={handleSubmit} type="primary">确定</Button>
        </div>
      </div>
    </div>
  );
};

export default connect(s => s.app)(LogIn);
