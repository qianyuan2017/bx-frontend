import { mine } from '../../utils/routes';

export const Mine = {
  path: mine.index,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./mine'));
    });
  },
};

export const LogIn = {
  path: mine.login,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./login'));
    });
  },
};

export const GetPassword = {
  path: mine.getPassword,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./getpassword'));
    });
  },
};

export const SalaryList = {
  path: mine.salary.list,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./salarylist'));
    });
  },
};

export const SalaryDetail = {
  path: `${mine.salary.detail}/:dt`,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./salarydetail'));
    });
  },
};

export const TimeBook = {
  path: mine.timebook,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./timebook'));
    });
  },
};
