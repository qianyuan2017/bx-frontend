import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Helmet } from 'react-helmet';
// import 'rc-calendar/assets/index.css';
import Calendar from 'rc-calendar';
import locale from 'rc-calendar/lib/locale/zh_CN';
import 'moment/locale/zh-cn';

const today = moment();
const danger = {
  color: '#fff',
  backgroundColor: '#d9534f',
  borderColor: '#d43f3a',
};
const warning = {
  color: '#fff',
  backgroundColor: '#f0ad4e',
  borderColor: '#eea236',
};

class TimeBook extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      day: this.props.day || moment(),
      currentMonth: (this.props.day || moment()).format('YYYY-MM'),
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      day: nextProps.day || this.state.day,
      currentMonth: (nextProps.day || this.state.day).format('YYYY-MM'),
    });
  }

  hanldeCalendarChange = (date) => {
    const mon = date.format('YYYY-MM');
    if (mon !== this.state.currentMonth) {
      this.props.dispatch({ type: 'timebook/list', payload: { month: mon } });
    }

    this.setState({ day: date, currentMonth: mon });
  };

  render() {
    const renaderCalendarCell = (dt) => {
      const bookDay = dt.format('YYYYMMDD');
      const morning = dt.isAfter(today) || (this.props.list || []).filter(x => x.rlbrcrddat === bookDay && x.key === 'A')[0];
      const night = dt.isAfter(today) || (this.props.list || []).filter(x => x.rlbrcrddat === bookDay && x.key === 'B')[0];

      const style = morning && night ? {} : (morning || night ? warning : danger);

      return (
        <div
          className="rc-calendar-date"
          style={style}
        >
          {
            dt.format('D')
          }
        </div>
      );
    };

    return (
      <div>
        <Helmet>
          <title>考勤表</title>
        </Helmet>
        <div>
          <p style={{ padding: '.64rem .64rem .40rem', margin: 0, fontSize: '2em' }}>
            考勤明细
          </p>
        </div>
        <Calendar
          locale={locale}
          showToday={false}
          showDateInput={false}
          onChange={this.hanldeCalendarChange}
          dateRender={renaderCalendarCell}
          renderFooter={() => {
            const smallBlock = {
              display: 'inline-block',
              padding: '.1rem',
              margin: '.04rem',
              verticalAlign: 'middle',
            };
            return (
              <small>
                红色<span style={{ ...danger, ...smallBlock }} />: 当天未打卡;
                黄色<span style={{ ...warning, ...smallBlock }} />: 上午或者下午未打卡.
              </small>
            );
          }}
        />
      </div>
    );
  }
}

export default connect(s => s.timebook)(TimeBook);
