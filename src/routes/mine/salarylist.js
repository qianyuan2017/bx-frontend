import React from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Helmet } from 'react-helmet';
import SList from '../../components/mine/salary/list';
import route from '../../utils/routes';

const SalaryList = (props) => {
  const handleItemClick = (it) => {
    props.dispatch(routerRedux.push(`${route.mine.salary.detail}/${it.prvdit}`));
  };

  return (
    <div>
      <Helmet>
        <title>工资条</title>
      </Helmet>
      <div>
        <p style={{ padding: '.64rem .64rem .40rem', margin: 0, fontSize: '2em' }}>
          工资明细
        </p>
      </div>
      <SList
        data={props.list}
        onItemClick={handleItemClick}
      />
    </div>
  );
};

export default connect(s => s.salary)(SalaryList);
