import React from 'react';
import { connect } from 'dva';
import { Helmet } from 'react-helmet';

const GetPassword = (props) => {
  return (
    <div>
      <Helmet>
        <title>找回密码</title>
      </Helmet>
      <div style={{ padding: '.256rem .32rem' }}>
        <p style={{ fontSize: '1.5em' }}>您的密码是</p>
        <p>{props.password}</p>
      </div>
    </div>
  );
};

export default connect(s => s.app.user)(GetPassword);
