import React from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Helmet } from 'react-helmet';
import MinePane from '../../components/mine';

const Mine = (props) => {
  const list = (props.list || []).map((it) => {
    return { ...it, onClick: () => props.dispatch(routerRedux.push(it.link)) };
  });

  return (
    <div style={{ height: '100%' }}>
      <Helmet>
        <title>我的</title>
      </Helmet>
      <MinePane
        list={list}
        avatar={props.avatar}
      />
    </div>
  );
};

export default connect(s => s.mine)(Mine);
