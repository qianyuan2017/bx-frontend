import React from 'react';
import { connect } from 'dva';
import { Helmet } from 'react-helmet';
import { WhiteSpace } from 'antd-mobile';
import SDetail from '../../components/mine/salary/detail';
import SDetailNew from '../../components/mine/salary/detailNew';

const SalaryDetail = (props) => {
  const dt = props.routeParams.dt || '';
  const title = `${dt.substr(0, 4)}年${dt.substr(-2)}月`;

  return (
    <div>
      <Helmet>
        <title>工资条</title>
      </Helmet>
      <div>
        <p style={{ padding: '.64rem .64rem .40rem', margin: 0, fontSize: '2em' }}>
          {title}
        </p>
      </div>
      {
        dt > 202205 ? <SDetailNew data={props.detail} /> : <SDetail data={props.detail} />
      }      
      <WhiteSpace size="lg" />
    </div>
  );
};

export default connect(s => s.salary)(SalaryDetail);
