import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import lodash from 'lodash';
import { Helmet } from 'react-helmet';
import { List, Picker, WhiteSpace, Flex } from 'antd-mobile';

const keyGen = (p, f) => `${p}-${f}`;

const styleDay = {
  color: '#000',
  backgroundColor: '#fff',
  borderColor: '#ddd',
};
const styleNight = {
  color: '#31708f',
  backgroundColor: '#d9edf7',
  borderColor: '#ddd',
};
const smallBlock = {
  display: 'inline-block',
  padding: '.1rem',
  margin: '.08rem',
  verticalAlign: 'middle',
};

const dateList = [
  lodash.times(3).map((i) => {
    const year = (new Date()).getFullYear();
    const val = year - i;
    return {
      label: `${val}年`,
      value: `${val}`,
    };
  }),
  lodash.times(12).map((i) => {
    const mon = i + 1;
    return {
      label: `${mon}月`,
      value: mon > 9 ? `${mon}` : `0${mon}`,
    };
  }),
];

class TimeBook extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentMonth: moment().format('YYYY-MM'),
    };
  }

  hanldeMonthChange = (date) => {
    const mon = `${date[0]}-${date[1]}`;
    if (mon !== this.state.currentMonth) {
      this.props.dispatch({ type: 'timebook/list', payload: { month: mon } });
    }

    this.setState({ day: date, currentMonth: mon });
  };

  render() {
    return (
      <div>
        <Helmet>
          <title>考勤表</title>
        </Helmet>
        <List renderHeader={() => (<p style={{ fontSize: '.34rem' }}>考勤明细</p>)}>
          <Picker
            data={dateList}
            title="选择月份"
            onChange={this.hanldeMonthChange}
            value={this.state.currentMonth.split('-')}
            extra="请选择"
            cascade={false}
          >
            <List.Item arrow="horizontal">选择月份</List.Item>
          </Picker>
        </List>
        <WhiteSpace size="lg" />
        {
          (!this.props.list || !this.props.list.length) &&
          (<p style={{ padding: '0 .48rem' }}>无数据!</p>)
        }
        <List>
          {
            (this.props.list || []).map((it, inx) => {
              const isNight = (it.key === 'A' && it.rlbrcrdtm.substr(0, 2) > '12') ||
                (it.key === 'B' && it.rlbrcrdtm.substr(0, 2) < '12');

              const dateDetail = it.rlbrcrddat.split('');
              const timeDetail = it.rlbrcrdtm.split('');

              return (
                <List.Item key={keyGen('tb', inx)} style={isNight ? styleNight : styleDay}>
                  <Flex>
                    <Flex.Item>
                      {`${dateDetail[4]}${dateDetail[5]}-${dateDetail[6]}${dateDetail[7]}`}
                    </Flex.Item>
                    <Flex.Item>
                      {`${timeDetail[0]}${timeDetail[1]}:${timeDetail[2]}${timeDetail[3]}`}
                      {it.key === 'A' ? ' 上班' : ' 下班'}
                    </Flex.Item>
                    <Flex.Item>
                      {it.odupz} 厂区
                    </Flex.Item>
                  </Flex>
                </List.Item>
              );
            })
          }
        </List>
      </div>
    );
  }
}

export default connect(s => s.timebook)(TimeBook);
