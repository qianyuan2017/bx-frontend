import React from 'react';
import { connect } from 'dva';
import { TabBar } from 'antd-mobile';
import Layout from '../components/layout';

const App = (props) => {
  const handleItemPress = it => () => {
    if (it.onPress) {
      it.onPress(props.dispatch);
    }
  };

  const tabbar = props.tabbar || {};
  const renderTabItems = (tabbar.items || []).map((it) => {
    return (
      <TabBar.Item
        title={it.title}
        key={it.key}
        icon={it.icon}
        selectedIcon={it.selectedIcon}
        selected={it.key === tabbar.active}
        onPress={handleItemPress(it)}
      />
    );
  });

  return (
    <Layout title={props.pageTitle}>
      {props.children}
      <TabBar hidden={tabbar.hidden}>
        {renderTabItems}
      </TabBar>
    </Layout>
  );
};

export default connect(s => ({ ...s.app, loading: s.loading }))(App);
