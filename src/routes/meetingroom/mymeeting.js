import React from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { List, Tabs, Badge } from 'antd-mobile';
import { meetingroom } from '../../utils/routes';

const Item = List.Item;
const Brief = Item.Brief;
const TabPane = Tabs.TabPane;

const MyMeeting = (props) => {
  const renderMyMeetings = (it) => {
    const badgeInfo = {
      lable: '申请中',
      color: '#108ee9',
    };

    switch (it.status) {
      case '1':
        badgeInfo.lable = '通过';
        badgeInfo.color = '#21b68a';
        break;
      case '2':
        badgeInfo.lable = '未通过';
        badgeInfo.color = '#f96268';
        break;
      case '3':
        badgeInfo.lable = '取消';
        badgeInfo.color = '#888';
        break;
      default:
    }

    return (
      <Link
        key={it.appointId}
        to={`${meetingroom.meeting.detail}/${it.appointId}`}
      >
        <Item
          arrow="horizontal"
          extra={<Badge text={badgeInfo.lable} style={{ backgroundColor: badgeInfo.color, borderRadius: '12px' }} />}
        >
          {`${it.roomName}`}
          <Brief>{`${it.appointDate} ${it.beginTime}-${it.endTime}`}</Brief>
        </Item>
      </Link>
    );
  };

  return (
    <div>
      <Tabs>
        <TabPane tab="我申请的" key="1">
          <List>{props.myApplies.map(renderMyMeetings)}</List>
        </TabPane>
        <TabPane tab="需审批的" key="2">
          <List>{props.otherApplies.map(renderMyMeetings)}</List>
        </TabPane>
      </Tabs>
    </div>
  );
};

const state2Props = s => ({ ...s.optrecord });
export default connect(state2Props)(MyMeeting);
