import React from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import NewApply from '../../components/meetingroom/NewApply';
import { meetingroom as route } from '../../utils/routes';

const Apply = (props) => {
  const hanleSubmit = (data) => {
    props.dispatch({ type: 'optrecord/applyMeetingRoom', payload: { ...data, applyPerson: props.user.empid } });
  };

  const handleReset = () => {
    const redirectTo = `${route.meeting.list}`;
    props.dispatch(routerRedux.push(redirectTo));
  };

  const room = props.rooms[props.detail.roomCode] || {};
  const data = { ...props.detail, approvalPerson: room.manager, approvalPersonName: room.managerName };

  return (
    <NewApply
      data={data}
      personList={props.depts}
      onSubmit={hanleSubmit}
      onReset={handleReset}
    />
  );
};

const state2Props = s => ({
  ...s.optrecord,
  depts: s.app.depts,
  user: s.app.user,
});
export default connect(state2Props)(Apply);
