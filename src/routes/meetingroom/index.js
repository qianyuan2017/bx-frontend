import { meetingroom } from '../../utils/routes';

export const MeetingList = {
  path: meetingroom.meeting.list,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./optrecord'));
    });
  },
};

export const PreElection = {
  path: `${meetingroom.meeting.preelection}/:roomCode/:day`,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./preelection'));
    });
  },
};

export const ApplyRoom = {
  path: meetingroom.meeting.apply,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./apply'));
    });
  },
};

export const ApplyDetail = {
  path: `${meetingroom.meeting.detail}/:recId`,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./applydetail'));
    });
  },
};

export const Approval = {
  path: `${meetingroom.meeting.approval}/:recId`,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./approval'));
    });
  },
};

export const MyMettings = {
  path: `${meetingroom.meeting.mymeeting}`,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./mymeeting'));
    });
  },
};
