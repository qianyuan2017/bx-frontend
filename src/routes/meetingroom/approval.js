import React from 'react';
import { connect } from 'dva';
import { List, Switch, TextareaItem, Button, Modal, Toast } from 'antd-mobile';
import { createForm } from 'rc-form';
import { STATUS } from '../../utils/consts';

const Approval = (props) => {
  const appointId = props.routeParams.recId;
  // const detail = (props.list || []).filter(x => x.appointId === appointId)[0] || {};
  const detail = props.detail || {};

  const meetingTime = `${detail.beginTime} - ${detail.endTime}`;
  const statusText = detail.statusText === '申请' ? '待审批' : detail.statusText;

  const handleApproval = () => {
    props.form.validateFields((err, values) => {
      if (err) {
        console.log(err);
        Modal.alert('错误', '审批出错, 请退出重试!');
        return undefined;
      }

      const data = { ...values, approvalPass: values.approvalPass ? '1' : '2' };

      props.dispatch({
        type: 'optrecord/approval',
        payload: data,
      });
    });
  };

  const cancelApply = () => {
    Modal.prompt(
      '确认取消?',
      '请输入取消原因',
      [
        { text: '不取消' },
        {
          text: '确认取消',
          onPress: v => new Promise((res, rej) => {
            if (!v) {
              Toast.offline('请输入取消原因');
              rej();
            }

            props.dispatch({
              type: 'optrecord/cancelApply',
              payload: {
                appointId,
                cancelPerson: props.user.empid,
                cancelReason: v,
              },
            });

            res();
          }),
        },
      ],
      'default',
    );
  };

  props.form.getFieldProps('appointId', { initialValue: appointId });

  return (
    <div>
      <List renderHeader={() => (<h2 style={{ fontWeight: 400 }}>主题: {detail.subject}</h2>)}>
        <List.Item extra={detail.roomName}>会议室</List.Item>
        <List.Item extra={detail.appointDate}>日期</List.Item>
        <List.Item extra={meetingTime}>时间</List.Item>
        <List.Item extra={statusText}>申请状态</List.Item>
      </List>
      <List renderHeader={() => '审批'}>
        <List.Item
          extra={(
            <Switch
              disabled={detail.status !== STATUS.ready}
              {...props.form.getFieldProps('approvalPass', {
                initialValue: (detail.approvalPass - 0) === 1,
                valuePropName: 'checked',
              })}
            />)}
        >是否同意</List.Item>
        <TextareaItem
          autoHeight
          placeholder="请在此处填写审批意见"
          rows={5}
          disabled={detail.status !== STATUS.ready}
          {...props.form.getFieldProps('approvalComment', {
            initialValue: detail.approvalComment || '',
          })}
        />
      </List>
      {
        detail.status === STATUS.ready &&
        (
          <div style={{ position: 'fixed', left: 0, bottom: 0, width: '100%' }}>
            <Button
              type="primary"
              onClick={handleApproval}
            >审批</Button>
          </div>
        )
      }
      {
        detail.status === STATUS.agree &&
        (
          <div style={{ position: 'fixed', left: 0, bottom: 0, width: '100%' }}>
            <Button
              type="primary"
              onClick={cancelApply}
            >取消</Button>
          </div>
        )
      }
    </div>
  );
};

const state2Props = s => ({ ...s.optrecord, user: s.app.user });
export default connect(state2Props)(createForm()(Approval));
