import React from 'react';
import { connect } from 'dva';
import { List, Button } from 'antd-mobile';
import { STATUS } from '../../utils/consts';

const ApplyDetail = (props) => {
  const appointId = props.routeParams.recId;
  // const detail = (props.list || []).filter(x => x.appointId === appointId)[0] || {};
  const detail = props.detail || {};

  const cancelApply = () => {
    props.dispatch({
      type: 'optrecord/delApply',
      payload: { appointId },
    });
  };

  const meetingTime = `${detail.beginTime} - ${detail.endTime}`;
  const statusText = detail.statusText === '申请' ? '待审批' : detail.statusText;

  return (
    <div>
      <List renderHeader={() => (<h2 style={{ fontWeight: 400 }}>主题: {detail.subject}</h2>)}>
        <List.Item extra={detail.roomName}>会议室</List.Item>
        <List.Item extra={detail.appointDate}>日期</List.Item>
        <List.Item extra={meetingTime}>时间</List.Item>
        <List.Item extra={statusText}>申请状态</List.Item>
      </List>
      {
        (detail.status === STATUS.agree || detail.status === STATUS.disAgree) &&
        (
          <List renderHeader={() => '审批结果'}>
            <List.Item extra={detail.approvalPersonName}>审批人</List.Item>
            <List.Item extra={detail.approvalDate}>审批时间</List.Item>
            <List.Item wrap extra={detail.approvalComment}>审批意见</List.Item>
          </List>
        )
      }
      {
        detail.status === STATUS.ready &&
        (
          <div style={{ position: 'fixed', left: 0, bottom: 0, width: '100%' }}>
            <Button
              type="primary"
              onClick={cancelApply}
            >删除申请</Button>
          </div>
        )
      }
    </div>
  );
};

const state2Props = s => ({ ...s.optrecord, user: s.app.user });
export default connect(state2Props)(ApplyDetail);
