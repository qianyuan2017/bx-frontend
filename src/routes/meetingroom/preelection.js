import React from 'react';
import moment from 'moment';
import _ from 'lodash';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { WingBlank, Button, Modal } from 'antd-mobile';
import XTimelineCalendar from '../../components/timeline/calendar';
import ListBanner from '../../components/meetingroom/ListBanner';
import { STATUS } from '../../utils/consts';
import { meetingroom as route } from '../../utils/routes';

const PreElection = (props) => {
  const { roomCode, day } = props.params;
  const roomName = props.rooms[roomCode].roomName;
  const date = moment(day);

  const interval = 0.5;
  const intvUnit = XTimelineCalendar.HOURS;
  const intvFormat = 'h';

  const data = (props.list[day][roomCode] || []).map((it) => {
    const st = it.beginTime;
    const et = it.endTime;

    const ctx = (
      <div style={{ padding: '16px 0' }}>
        <div>{`${it.applyPersonName} | ${st} - ${et}`}</div>
      </div>
    );

    const getMode = () => {
      // 已经失效的记录
      if (moment().isAfter(it.EndDate)) return XTimelineCalendar.MODE_DISABLE;

      switch (it.status) {
        case STATUS.ready:
          return XTimelineCalendar.MODE_INPROCESS;
        case STATUS.agree:
          return XTimelineCalendar.MODE_ACTIVE;
        case STATUS.disAgree:
        case STATUS.cancel:
          return XTimelineCalendar.MODE_DISABLE;
        default:
          return XTimelineCalendar.MODE_NORMAL;
      }
    };

    return {
      start: `${day} ${st}`,
      end: `${day} ${et}`,
      mode: getMode(),
      ctx,
    };
  });

  let scheduledStart = {};
  let scheduledEnd = {};
  const handleNextClick = () => {
    if (!scheduledStart || !scheduledEnd) {
      Modal.alert({ title: '提示', message: '请选择时间' });
      return undefined;
    }

    const detail = {
      roomCode,
      roomName,
      startDate: scheduledStart,
      endDate: scheduledEnd,
    };

    props.dispatch({ type: 'optrecord/syncState', payload: { detail } });
    props.dispatch(routerRedux.push(route.meeting.apply));
  };

  const handleSelect = (dtList) => {
    if (!dtList.length) return undefined;

    // 改为有序列
    // dtList 是依据下面 XTimelineCalendar.dateFormat 生成
    // 因此需要一个组合成为一个有效的日期
    // 这里借用的 2017-06-06
    const dtOrdered = _.sortBy(dtList, x => moment(`2017-06-06 ${x}`).valueOf());
    const checkStart = dtOrdered[0].split(':');
    const checkEnd = dtOrdered[dtOrdered.length - 1].split(':');
    scheduledStart = date.hour(checkStart[0]).minutes(checkStart[1]).toDate();

    // 这里需要补充 interval 的时长
    scheduledEnd = date.hour(checkEnd[0]).minutes(checkEnd[1]).add(interval, intvFormat).toDate();
  };

  const headStyle = {
    position: 'fixed',
    top: '0',
    width: '100%',
    zIndex: '999',
  };

  const bodyStyle = {
    margin: '128px 0',
    paddingTop: '32px',
    height: 'calc(100% -256px)',
  };

  const footStyle = {
    position: 'fixed',
    bottom: '16px',
    width: '100%',
    zIndex: '999',
  };

  return (
    <div style={{ height: '100%' }}>
      <div style={headStyle}>
        <ListBanner
          title={roomName}
          date={date}
        />
      </div>
      <div style={bodyStyle}>
        <XTimelineCalendar
          timeStart={date.hour(8).minutes(0).toDate()}
          timeEnd={date.hour(20).minutes(0).toDate()}
          interval={interval}
          intvUnit={intvUnit}
          dateFormat="HH:mm"
          data={data}
          onSelect={handleSelect}
        />
      </div>
      <div style={footStyle}>
        <WingBlank>
          <Button
            type="primary"
            onClick={handleNextClick}
          >下一步</Button>
        </WingBlank>
      </div>
    </div>
  );
};

const state2Props = s => ({ ...s.optrecord });
export default connect(state2Props)(PreElection);
