import React from 'react';
import lodash from 'lodash';
import moment from 'moment';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Tabs } from 'antd-mobile';
import ListBanner from '../../components/meetingroom/ListBanner';
import HozShow from '../../components/timeline/hoz-show';
import { meetingroom as route } from '../../utils/routes';

class OptRecord extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeKey: '0',
      fetchNo: 0,
    };
  }

  componentWillReceiveProps(nextProps) {
    const k = (nextProps.weekdays || []).indexOf(moment().format('YYYY-MM-DD'));

    if (k > -1) {
      this.setState({ activeKey: `${k}` });
    }
  }

  handleBannerChange = (dt) => {
    // dt 是 moment 类型
    this.props.dispatch({ type: 'optrecord/refreshRecord', payload: { date: dt, st: '' } });
  };

  handleTabClick = (k) => {
    this.setState({ activeKey: k });
    const { fetchNo } = this.state;

    if (this.props.weekdays.length <= 7 && (k - 0) >= 5 && fetchNo < 1) {
      this.setState({ fetchNo: 1 });

      // const nextMonday = moment(this.props.weekdays[0]).add(7, 'days').format('YYYY-MM-DD');
      // this.props.dispatch({ type: 'optrecord/getSchedulesByWeek', payload: { monday: nextMonday } });
    }
  };

  handleBarClick = ({ roomCode, day }) => {
    const redirectTo = `${route.meeting.preelection}/${roomCode}/${day}`;
    this.props.dispatch(routerRedux.push(redirectTo));
  };

  render() {
    const renderTabPanes = () => {
      // 日期列表
      return (this.props.weekdays || []).map((day, inx) => {
        const start = moment(day).hour(8).minute(0).toDate();
        const end = moment(day).hour(20).minute(0).toDate();

        // 会议列表
        const data = lodash.map(this.props.list[day], (recList, roomCode) => {
          const { roomName } = this.props.rooms[roomCode];
          if (!recList) return { ctx: roomName, list: [], roomCode, day };

          // 申请记录
          const list = recList.map(rec => ({
            start: moment(`${day} ${rec.beginTime}`).toDate(),
            end: moment(`${day} ${rec.endTime}`).toDate(),
          }));

          return { ctx: roomName, list, roomCode, day };
        }).filter(x => !!x);

        const genKey = () => `${inx}`;

        const dayName = moment(day).format('MM月DD日');

        return (
          <Tabs.TabPane tab={dayName} key={genKey()}>
            <HozShow
              key={day}
              timeStart={start}
              timeEnd={end}
              interval={0.5}
              intvUnit={HozShow.HOURS}
              data={data}
              onRowClick={this.handleBarClick}
              titleSpan={2}
              titleShow="HH:mm"
            />
          </Tabs.TabPane>
        );
      });
    };

    const renderTabs = () => {
      return (
        <Tabs activeKey={this.state.activeKey} pageSize={5} onTabClick={this.handleTabClick}>
          {renderTabPanes()}
        </Tabs>
      );
    };

    return (
      <div>
        <ListBanner
          title="会议室系统"
          date={this.props.date}
          onChange={this.handleBannerChange}
        />
        {renderTabs()}
      </div>
    );
  }
}

const state2Props = s => ({ ...s.optrecord, resource: s.resource });
export default connect(state2Props)(OptRecord);
