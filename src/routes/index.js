import route from '../utils/routes';
import App from './app';
import { NewsDetail, NewsList } from './company';
import { Mine, LogIn, GetPassword, SalaryList, SalaryDetail, TimeBook } from './mine';
import { MeetingList, PreElection, ApplyRoom, ApplyDetail, MyMettings, Approval } from './meetingroom';

export default (app) => {
  const appInit = () => {
    app._store.dispatch({ type: 'app/appInit' }); // eslint-disable-line
  };

  return {
    path: route.index,
    onEnter: appInit,
    component: App,
    // indexRoute: { component: NewsList },
    childRoutes: [
      NewsDetail,
      NewsList,
      LogIn,
      GetPassword,
      SalaryList,
      SalaryDetail,
      MeetingList,
      PreElection,
      ApplyRoom,
      ApplyDetail,
      Approval,
      MyMettings,
      Mine,
      TimeBook,
    ],
  };
};
