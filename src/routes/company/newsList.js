import React from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Helmet } from 'react-helmet';
import { Tabs } from 'antd-mobile';
import NList from '../../components/company/news/list';
import { company } from '../../utils/routes';

const TabPane = Tabs.TabPane;

const NewsList = (props) => {
  const regx = /type=([^?&#]+)/i;
  const match = regx.exec(props.location.search);
  const newsType = match ? match[1] : '1';
  const { data = [] } = props.list.filter(x => `${x.type}` === `${newsType}`)[0] || {};

  const handleItemClick = (it) => {
    props.dispatch({ type: 'news/syncState', payload: { detail: it.raw } });
    props.dispatch(routerRedux.push(`${company.news.detail}/${it.newsId}`));
  };

  const handleTabChange = (tabKey) => {
    props.dispatch(routerRedux.push(`${company.news.list}?type=${tabKey}`));
    // const dt = props.list.filter(x => x.type === key)[0];
    // if (!dt) {
    //   props.dispatch({ type: 'news/listOnLoad', payload: { type: key, pageNo: 1, pageSize: 999 } });
    // }
  };

  const handleRefresh = type => (callback) => {
    props.dispatch({ type: 'news/listOnLoad', payload: { type, callback } });
  };

  return (
    <div className="bx-news">
      <Helmet>
        <title>新闻列表</title>
      </Helmet>
      <div style={{ height: '100%' }}>
        <Tabs activeKey={newsType} onChange={handleTabChange}>
          {
            props.newsType.map(it => (<TabPane tab={it.label} key={`${it.value}`} />))
          }
        </Tabs>
        <NList
          key={`list-${newsType}`}
          data={data}
          onItemClick={handleItemClick}
          onRefresh={handleRefresh(newsType)}
        />
      </div>
    </div>
  );
};

export default connect(s => s.news)(NewsList);
