import { company } from '../../utils/routes';

export const NewsDetail = {
  path: `${company.news.detail}/:newsId`,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./newsDetail'));
    });
  },
};

export const NewsList = {
  path: company.news.list,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      cb(null, require('./newsList'));
    });
  },
};
