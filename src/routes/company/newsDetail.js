import React from 'react';
import { connect } from 'dva';
import { Helmet } from 'react-helmet';
import NDetail from '../../components/company/news/detail';

const NewsDetail = (props) => {
  // const newsId = (props.routeParams.newsId || '') - 0;
  // const typeId = props.location.query.type;

  // const list = typeId ? (props.list || []).filter(x => `${x.type}` === `${typeId}`)[0] : (props.list || [])[0];
  // const data = ((list || {}).data || []).filter(x => `${x.newsId}` === `${newsId}`)[0];

  return (
    <div>
      <Helmet>
        <title>新闻详情</title>
      </Helmet>
      <NDetail data={props.detail} />
    </div>
  );
};

export default connect(s => s.news)(NewsDetail);
