import dva from 'dva';
import { browserHistory, hashHistory } from 'dva/router';
import { Modal } from 'antd-mobile';
import addModelsFor from './models';
import './index.css';

const history = process.env.NODE_ENV === 'development' ?
  hashHistory :
  browserHistory;

// 1. Initialize
const app = dva({
  history,
  onError: (e) => {
    Modal.alert('错误', e.message);
  },
  // onEffect: (effect) => {
  //   return function *(...args) {
  //     Toast.loading('请稍后', 0);
  //     yield effect(...args);
  //     Toast.hide();
  //   };
  // },
});

// 2. Plugins
// app.use();

// 3. Model
addModelsFor(app);

// 4. Router
app.router(require('./router'));

// 5. Start
app.start('#root');
