import React from 'react';
import QueueAnim from 'rc-queue-anim';
import { List } from 'antd-mobile';
import STag from './simpleTag';

// min - max 整型随机
const rand = (min, max) => {
  return Math.floor((Math.random() * ((max - min) + 1)) + min);
};

const tagStyle = ['primary', 'success', 'info', 'warning', 'danger'];

export default (props) => {
  const handleItemClick = it => () => {
    if (props.onItemClick) {
      props.onItemClick(it);
    }
  };

  const genList = (props.list || []).map((it, inx) => {
    const genKey = () => `item-${inx}`;
    const { tag, label } = it || {};
    const tagType = tagStyle[rand(0, 4)];

    return (
      <List.Item key={genKey()} onClick={handleItemClick(it)}>
        <STag type={tagType} style={{ margin: '0 16px' }}>
          {tag}
        </STag>
        {label}
      </List.Item>
    );
  });

  return (
    <div>
      <List>
        <QueueAnim
          animConfig={[
            { opacity: [1, 0], translateY: [0, 30] },
            { height: 0 },
          ]}
          ease={['easeOutQuart', 'easeInOutQuart']}
          duration={[550, 450]}
          interval={150}
        >
          {
            props.title &&
            (
              <List.Item arrow="horizontal">
                {props.title}
              </List.Item>
            )
          }
          {genList}
        </QueueAnim>
      </List>
    </div>
  );
};
