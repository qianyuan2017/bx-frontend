
const dullOx = (conf) => {
  const defConf = {
    data: undefined,
    submit: undefined,
  };

  if (typeof conf.submit !== 'function') {
    throw new Error('Param submit must be a function');
  }

  const config = Object.assign(defConf, conf);

  const effect = (cb) => {
    return new Proxy(cb, {
      apply: (t, o, args) => {
        Function.prototype.apply.call(t, o, args);

        const submitData = typeof data === 'function' ? config.data() : config.data;
        config.submit(submitData);
      },
    });
  };

  return effect;
};

export default dullOx;
