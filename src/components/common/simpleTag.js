import React from 'react';

const styleBasic = {
  display: 'inline-block',
  position: 'relative',
  fontSize: '.28rem',
  textAlign: 'center',
  padding: '0 .3rem',
  height: '.5rem',
  lineHeight: '.5rem',
  borderRadius: '.06rem',
  boxSizing: 'border-box',
};

const styleMode = {
  default: {
    backgroundColor: '#e6e6e6',
    color: '#333',
    border: '1px solid #adadad',
  },
  primary: {
    backgroundColor: '#286090',
    color: '#fff',
    border: '1px solid #204d74',
  },
  success: {
    backgroundColor: '#5cb85c',
    color: '#fff',
    border: '1px solid #4cae4c',
  },
  info: {
    backgroundColor: '#5bc0de',
    color: '#fff',
    border: '1px solid #46b8da',
  },
  warning: {
    backgroundColor: '#f0ad4e',
    color: '#fff',
    border: '1px solid #eea236',
  },
  danger: {
    backgroundColor: '#d9534f',
    color: '#fff',
    border: '1px solid #d43f3a',
  },
};

export default (props) => {
  const style = props.type ? styleMode[props.type] : styleMode['default'];

  return (
    <span style={{ ...styleBasic, ...style, ...props.style }}>
      {props.children}
    </span>
  );
};
