import React from 'react';
import PropTypes from 'prop-types';
import lodash from 'lodash';
import { List, Button, Checkbox, SearchBar, Flex } from 'antd-mobile';
import HeadImg from './headimg';
import { defaultAvatar, defaultDept } from './defaultphoto';

const MODE_NOCHECK = 0;
const MODE_CHECK_USER = 1 << 0;
const MODE_CHECK_DEPT = 1 << 1;
const TYPE_USER = 1;
const TYPE_DEPT = 2;

const genKey = (f, k) => `x-contacts-${f}-${k}`;

class ContactSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contacts: this.props.data,
      checkList: this.props.checkList || [],
      history: [],
      searchText: '',
      nodeList: this.getNodeList(this.props.data),
    };
  }

  componentWillReceiveProps(nextprops) {
    this.setState({
      contacts: nextprops.data,
      checkList: nextprops.checkList,
      nodeList: this.getNodeList(nextprops.data),
    });
  }

  getNodeList = (tree, parent) => {
    return (tree || []).reduce((acc, node) => {
      const nodeWithParent = { ...node, parent };
      const newAcc = [...acc, lodash.omit(nodeWithParent, 'children')];

      return node.children && node.children.length ?
        [...newAcc, ...this.getNodeList(node.children, node.id)] :
        newAcc;
    }, []);
  }

  getNodeDeep = (nodes, id, type) => {
    let res;
    if (Array.isArray(nodes)) {
      (nodes || []).forEach((x) => {
        if (res) return;

        const tmpRes = this.getNodeDeep(x, id, type);
        if (tmpRes) res = tmpRes;
      });
      return res;
    }

    const { id: nid, type: ntyp, children, users } = nodes;
    if (`${nid}` === `${id}` && `${ntyp}` === `${type}`) return nodes;

    if (users && users.length) {
      res = this.getNodeDeep(users, id, type);
    }

    if (!res && children && children.length) {
      res = this.getNodeDeep(children, id, type);
    }

    return res;
  }

  getAvatar = (it) => {
    if (it.avatar) return it.avatar;
    switch (it.type) {
      case TYPE_DEPT:
        return defaultDept;
      default:
        return defaultAvatar;
    }
  }

  handleSearch = (val) => {
    if (val.length) {
      if (this.props.searchFilter) {
        const contacts = this.state.nodeList.filter(x => this.props.searchFilter(x.raw, val));
        this.setState({ contacts });
      }
    } else {
      this.setState({ contacts: this.props.data });
    }
  }

  handleSearchChange = (searchText) => {
    this.setState({ searchText });

    if (!searchText) {
      this.handleSearchCancel();
    }
  }

  handleSearchCancel = () => {
    this.setState({ searchText: '', contacts: this.props.data });
  }

  handleHistoryBack = () => {
    const len = this.state.history.length;
    if (len < 1) return undefined;

    const { contacts } = this.state.history[len - 1];
    const history = this.state.history.slice(0, -1);
    this.setState({ contacts, history });
  };

  handleItemClick = node => () => {
    if (node.children && node.children.length) {
      const contacts = [...this.state.contacts];
      const nodeWithoutChildren = lodash.omit(node, 'children');
      const history = [...this.state.history, { contacts, node: nodeWithoutChildren }];

      this.setState({
        history,
        contacts: node.children,
      });
    }

    if (this.props.onItemClick) {
      this.props.onItemClick(node);
    }
  }

  handleItemCheck = node => (e) => {
    const checked = e.target.checked;

    if (checked) {
      const checkList = [...this.state.checkList, node.id];
      this.setState({ checkList }, () => {
        // 如果是单选模式, 选择一个即提交
        if (this.props.checkMulti === false) {
          if (this.state.checkList.length === 1) {
            this.handleSubmit();
          }
        }
      });
    } else {
      const checkList = this.state.checkList.filter(x => x !== node.id);
      this.setState({ checkList });
    }
  }

  handleSubmit = () => {
    if (this.props.onSubmit) {
      const { checkList } = this.state;
      const dts = (checkList || []).map((x) => {
        return this.state.nodeList.filter(it => it.id === x)[0].raw;
      });

      this.props.onSubmit(dts);
    }
  }

  render() {
    const { checkList, contacts, history } = this.state;

    // 列表
    const renderList = () => {
      const { node } = history.slice(-1)[0] || {};
      const headName = node ? `< ${node.name}` : '请选择';

      return (
        <List
          className="x-contacts-list"
          renderHeader={(
            <div onClick={this.handleHistoryBack}>
              <span style={{ marginLeft: '.08rem', fontSize: '.34rem' }}>{headName}</span>
            </div>
          )}
        >
          {
            contacts.map((it, inx) => {
              const checked = checkList.indexOf(it.id) > -1;
              const hasChildren = !!it.children && it.children.length;
              const checkItem = (() => {
                switch (this.props.mode) {
                  case MODE_CHECK_USER:
                    return it.type === TYPE_USER;
                  case MODE_CHECK_DEPT:
                    return it.type === TYPE_DEPT;
                  default:
                    return false;
                }
              })();

              const extraClick = hasChildren ? this.handleItemClick(it) : undefined;
              const arrow = hasChildren ? 'horizontal' : 'empty';
              const avatar = this.getAvatar(it);

              return (
                <List.Item key={genKey('item', inx)} arrow={arrow}>
                  {
                    checkItem ?
                    (
                      <Flex>
                        <Checkbox.CheckboxItem
                          checked={checked}
                          onChange={this.handleItemCheck(it)}
                        >
                          <HeadImg avatar={avatar} style={{ width: '.48rem', height: '.48rem', verticalAlign: 'top' }} />
                        </Checkbox.CheckboxItem>
                        <div onClick={extraClick} style={{ width: 'calc(100% - 2.2rem)', marginLeft: '.08rem' }}>{it.name}</div>
                      </Flex>
                    ) :
                    (
                      <div onClick={extraClick}>
                        <HeadImg avatar={avatar} style={{ width: '.48rem', height: '.48rem', verticalAlign: 'middle' }} />
                        <span style={{ marginLeft: '.08rem' }}>{it.name}</span>
                      </div>
                    )
                  }
                </List.Item>
              );
            })
          }
        </List>
      );
    };

    return (
      <div style={{ height: '100%', backgroundColor: '#fff' }}>
        {
          !(this.props.search === false) &&
          (
            <SearchBar
              placeholder="搜索"
              value={this.state.searchText}
              onSubmit={this.handleSearch}
              onChange={this.handleSearchChange}
              onCancel={this.handleSearchCancel}
            />
          )
        }
        <div style={{ height: 'calc(100% - 1.7rem)', overflowY: 'scroll' }}>
          {contacts.length ? renderList(contacts) : (<div style={{ padding: '.4rem' }} >无内容</div>)}
        </div>
        <div style={{ position: 'fixed', bottom: '0', width: '100%', borderTop: '.02rem solid #ccc' }}>
          <List>
            <List.Item
              extra={
                this.props.onSubmit &&
                (
                  <Button onClick={this.handleSubmit} type="ghost" size="small" inline>
                    {`确定(${checkList.length})`}
                  </Button>
                )
              }
            >
              {
                checkList.map((it, inx) => {
                  const node = this.state.nodeList.filter(x => x.id === it)[0];
                  if (!node) return undefined;

                  const avatar = this.getAvatar(node);

                  return (
                    <HeadImg key={genKey('avatar', inx)} avatar={avatar} size={0.48} />
                  );
                })
              }
            </List.Item>
          </List>
        </div>
      </div>
    );
  }
}

// data 是对象数组
// 对象中要求有 id, avatar, name, type, children, raw 字段, 其中 raw 为对应节点的原始数据
// type = 1 为 user, 2 为 dept
ContactSelect.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  mode: PropTypes.number,
  search: PropTypes.bool,
  checkMulti: PropTypes.bool,
  onItemClick: PropTypes.func,
  onSubmit: PropTypes.func,
  searchFilter: PropTypes.func,
};

ContactSelect.MODE_NOCHECK = MODE_NOCHECK;
ContactSelect.MODE_CHECK_USER = MODE_CHECK_USER;
ContactSelect.MODE_CHECK_DEPT = MODE_CHECK_DEPT;
ContactSelect.TYPE_USER = TYPE_USER;
ContactSelect.TYPE_DEPT = TYPE_DEPT;

export default ContactSelect;
