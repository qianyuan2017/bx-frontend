import React from 'react';
import classnames from 'classnames';
import { defaultAvatar } from './defaultphoto';
import Style from './style.less';

export default (props) => {
  const size = props.size || 128;
  const outerSt = props.style || { width: `${size}rem`, height: `${size}rem` };

  const avatar = props.avatar || defaultAvatar;
  const innerSt = { backgroundImage: `url(${avatar})` };

  const basicEle = (
    <span
      className={classnames(
        Style['x-headimg'],
        props.photoCls,
      )}
      style={outerSt}
    >
      <span style={innerSt} />
    </span>
  );

  return props.name ? (
    <div
      className={classnames(
        Style['x-headimg-title'],
        props.warpCls,
      )}
    >
      {basicEle}
      <span className={Style['x-headimg-name']}>{props.name}</span>
    </div>
  ) : basicEle;
};
