import React from 'react';
import moment from 'moment';
import { Flex, DatePicker } from 'antd-mobile';
import { thickCalendarWhite } from './icon';

// 转换周天为中文
const transWeekDay = (n) => {
  const d = '\u4e00,\u4e8c,\u4e09,\u56db,\u4e94,\u516d,\u4e03'.split(',');
  return `\u661f\u671f${d[n - 1]}`;
};

class ListBanner extends React.Component {
  constructor(props) {
    super(props);

    // date 属性必须是 momnet 类型
    const date = this.props.date || moment(new Date());
    this.state = {
      title: this.props.title,
      date,
      visible: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    const date = nextProps.date || (new Date());
    this.setState({
      title: nextProps.title,
      date,
      visible: false,
    });
  }

  handleCalendarChange = (date) => {
    const visible = false;
    this.setState({ date, visible });

    if (this.props.onChange) {
      this.props.onChange(date);
    }
  }

  handleCalendarClick = () => {
    // this.setState({
    //   visible: true,
    // });
  }

  handleCloseDatePicker = () => {
    const visible = false;
    this.setState({ visible });
  }

  render() {
    const calendarStyle = {
      textAlign: 'center',
      width: '50%',
    };

    const bannerStyle = this.props.style || {
      padding: '8px 0',
      backgroundColor: '#1272B8',
      color: '#fff',
    };

    const XCalendar = (props) => {
      return (
        <Flex>
          <div style={calendarStyle}>
            <img
              width="100"
              onClick={this.handleCalendarClick}
              src={thickCalendarWhite} alt=""
            />
          </div>
          <div style={calendarStyle}>
            <div>{this.state.date.format('YYYY-MM-DD')}</div>
            <div>{transWeekDay(this.state.date.format('E'))}</div>
          </div>
        </Flex>
      );
    };

    return (
      <div style={bannerStyle}>
        <Flex>
          <Flex.Item>
            <DatePicker
              mode="date"
              visible={this.state.visible}
              title="请选择日期"
              onDismiss={this.handleCloseDatePicker}
              onChange={this.handleCalendarChange}
            >
              <XCalendar />
            </DatePicker>
          </Flex.Item>
          <Flex.Item>
            <div style={{ textAlign: 'center' }}>{this.state.title}</div>
          </Flex.Item>
        </Flex>
      </div>
    );
  }
}

export default ListBanner;
