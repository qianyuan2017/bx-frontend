import React from 'react';
import moment from 'moment';
import lodash from 'lodash';
import { createForm } from 'rc-form';
import {
  List,
  InputItem,
  Button,
  Flex,
  Modal,
  WingBlank,
} from 'antd-mobile';
import ContactSelect from '../contact-select';

const arr2Contact = keymap => (arr) => {
  const { id, avatar, name, pId, type } = keymap;
  const isFunc = f => typeof f === 'function';

  if (!arr || !arr.length) {
    return undefined;
  }

  const arrBeforeTransfer = arr.map((it) => {
    return {
      id: isFunc(id) ? id(it) : it[id],
      avatar: isFunc(avatar) ? avatar(it) : it[avatar],
      name: isFunc(name) ? name(it) : it[name],
      pId: isFunc(pId) ? pId(it) : it[pId],
      type: isFunc(type) ? type(it) : it[type],
      raw: it,
    };
  });

  const pTrees = [];
  const tree = arrBeforeTransfer.map((it, inx, dt) => {
    const hasParent = dt.filter(x => x.id === it.pId);

    if (hasParent.length) {
      if (!hasParent[0].children) {
        hasParent[0].children = [];
      }
      hasParent[0].children.push(it);
    } else {
      const inPTree = pTrees.filter(x => x.id === it.pId);
      if (inPTree.length) {
        inPTree[0].children.push(it);
      } else {
        return it;
      }
    }

    return undefined;
  });

  return tree.filter(x => x);
};

const depts2Contact = arr2Contact({
  id: 'code',
  name: 'name',
  avatar: () => undefined,
  pId: 'parentCode',
  type: x => (x.personSign === '1' ? ContactSelect.TYPE_USER : ContactSelect.TYPE_DEPT),
});

class NewApply extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      managerShow: false,
      approvalPersonShow: false,
      manager: '',
    };
  }

  // componentWillReceiveProps(nextProps) {
  //   this.setState({
  //     managerShow: false,
  //   });
  // }

  handlePersonClick = who => () => {
    const show = { [who]: true };

    this.setState({ ...show });
  }

  handlePersonCheck = who => (persons) => {
    const name = persons.reduce((acc, it) => {
      acc.push(it.name);
      return acc;
    }, []).join(',');

    if (persons && persons.length) {
      this.props.form.setFieldsValue({ [who]: persons[0].code });
    }

    this.setState({
      [`${who}Show`]: false,
      [who]: name,
    });
  }

  handleSubmit = () => {
    if (this.props.onSubmit) {
      this.props.form.validateFields((err, values) => {
        if (err) {
          const errMsg = lodash.flatten(lodash.map(err, 'errors')).map(x => x.message).join(';');
          Modal.alert('错误', errMsg);
          return undefined;
        }

        this.props.onSubmit(values);
      });
    }
  }

  handleReset = () => {
    this.props.onReset();
  }

  render() {
    const { form, personList, data: { roomCode, roomName, startDate, endDate, approvalPerson, approvalPersonName } } = this.props;

    const persons = depts2Contact(personList);
    const appointDate = moment(startDate).format('YYYY-MM-DD');
    const beginTime = moment(startDate).format('HH:mm');
    const endTime = moment(endDate).format('HH:mm');

    const personListStyle = {
      position: 'fixed',
      top: '0',
      zIndex: '999',
      width: '100%',
      height: '100%',
      backgroundColor: '#fff',
    };

    // 隐藏字段
    form.getFieldProps('roomCode', { initialValue: roomCode });
    form.getFieldProps('appointDate', { initialValue: appointDate });
    form.getFieldProps('beginTime', { initialValue: beginTime });
    form.getFieldProps('endTime', { initialValue: endTime });
    form.getFieldProps('manager');
    form.getFieldProps('approvalPerson', { initialValue: approvalPerson });

    return (
      <div>
        {
          this.state.managerShow &&
          (
            <div style={personListStyle}>
              <ContactSelect
                data={persons}
                checkMulti={false}
                mode={ContactSelect.MODE_CHECK_USER}
                onSubmit={this.handlePersonCheck('manager')}
              />
            </div>
          )
        }
        {
          this.state.approvalPersonShow &&
          (
            <div style={personListStyle}>
              <ContactSelect
                data={persons}
                checkMulti={false}
                mode={ContactSelect.MODE_CHECK_USER}
                onSubmit={this.handlePersonCheck('approvalPerson')}
              />
            </div>
          )
        }
        <List>
          <InputItem
            editable={false}
            value={roomName}
          >会议室</InputItem>
          <InputItem
            editable={false}
            value={appointDate}
          >预订日期</InputItem>
          <InputItem
            editable={false}
            value={`${beginTime} - ${endTime}`}
          >预订时间</InputItem>
          <InputItem
            clear
            {...form.getFieldProps('subject', {
              rules: [{ required: true, message: '请输入主题' }],
            })}
          >主题</InputItem>
          <InputItem
            editable={false}
            value={this.state.manager}
            onClick={this.handlePersonClick('managerShow')}
          >会务管理</InputItem>
          <InputItem
            editable={false}
            value={approvalPersonName}
          >审批人</InputItem>
        </List>
        <div style={{ padding: '64px 0' }}>
          <Flex>
            <Flex.Item>
              <WingBlank size="md">
                <Button
                  style={{ width: '100%' }}
                  type="primary"
                  inline
                  onClick={this.handleSubmit}
                >确定</Button>
              </WingBlank>
            </Flex.Item>
            <Flex.Item>
              <WingBlank size="md">
                <Button
                  style={{ width: '100%' }}
                  type="ghost"
                  inline
                  onClick={this.handleReset}
                >重选</Button>
              </WingBlank>
            </Flex.Item>
          </Flex>
        </div>
      </div>
    );
  }
}

const DetailWithForm = createForm()(NewApply);
export default DetailWithForm;
