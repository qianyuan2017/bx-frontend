import React from 'react';

export default (props) => {
  return (
    <div style={{ height: '100%' }}>
      {props.children}
    </div>
  );
};
