import React from 'react';
import moment from 'moment';
import PullToRefresh from 'rmc-pull-to-refresh';
import { List } from 'antd-mobile';
import STag from '../../common/simpleTag';

// min - max 整型随机
const rand = (min, max) => {
  return Math.floor((Math.random() * ((max - min) + 1)) + min);
};

const genKey = (p, f) => `${p}-${f}`;

const tagStyle = ['primary', 'success', 'info', 'warning', 'danger'];

class NewsList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      refreshing: false,
    };
  }

  handleItemClick = it => () => {
    if (this.props.onItemClick) {
      this.props.onItemClick(it);
    }
  }

  handleRefresh = () => {
    if (this.props.onRefresh) {
      this.setState({ refreshing: true });
      this.props.onRefresh(this.setState({ refreshing: false }));
    }
  }

  render() {
    const listItems = (this.props.data || []).map((it) => {
      return {
        tag: moment(it.pubDate).format('MM-DD'),
        label: it.subject,
        newsId: it.newsId,
        raw: it,
      };
    });

    const indicator = {
      activate: '上拉加载数据',
      deactivate: 'pull',
      release: '加载中...',
      finish: '完成',
    };

    return (
      <PullToRefresh
        direction="up"
        indicator={indicator}
        refreshing={this.state.refreshing}
        onRefresh={this.handleRefresh}
      >
        {
          listItems.length === 0 &&
          (<div style={{ padding: '1rem', color: '#888', textAlign: 'center' }}>暂无数据</div>)
        }
        <List style={{ paddingBottom: '.20rem' }}>
          {listItems.map((item, inx) => {
            return (
              <List.Item key={genKey('item', inx)} onClick={this.handleItemClick(item)}>
                <STag type={tagStyle[rand(0, 4)]} style={{ margin: '0 16px' }}>
                  {item.tag}
                </STag>
                {item.label}
              </List.Item>
            );
          })}
        </List>
      </PullToRefresh>
    );
  }
}

export default NewsList;
