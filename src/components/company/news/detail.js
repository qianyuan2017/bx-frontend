import React from 'react';
import moment from 'moment';
import Style from './style.less';

const pipe = (f, g) => (...args) => g.call(this, f.apply(this, args));
const compose = (...args) => args.reverse().reduce(pipe, args.shift());

export default (props) => {
  const data = props.data || {};
  const trimPghSpace = (raw) => {
    const regx = /(<p[^>]*>)(\s|　)*([^<]*<\/p>)/ig;
    return raw.replace(regx, '$1$3');
  };
  const trimSpanSpace = (raw) => {
    const regx = /(<span[^>]*>)(\s|　)*([^<]*<\/span>)/ig;
    return raw.replace(regx, '$1$3');
  };

  const trimEnter = (raw) => {
    return raw.replace(/[\r\n]/g, '');
  };

  const replaceFontSize = (raw) => {
    return raw.replace(/font-size:\s?[^; "]+/ig, 'font-size: 48rpx');
  };

  const trimLineHeight = (raw) => {
    return raw.replace(/line-height:\s?[^; "]+/ig, '');
  };

  const trimTextIndent = (raw) => {
    return raw.replace(/text-indent:\s?[^; "]+/ig, '');
  };

  const delSpam = compose(
    trimTextIndent,
    trimLineHeight,
    trimSpanSpace,
    trimPghSpace,
    replaceFontSize,
    trimEnter,
  );

  return (
    <article className={Style['news']}>
      <div className={Style['news-head']}>
        <h1 className={Style['news-title']}>{data.subject}</h1>
        <div className={Style['news-subtitle']}>
          <ul>
            <li>{data.author}</li>
            <li>{moment(data.pubDate).format('YYYY-MM-DD')}</li>
          </ul>
        </div>
      </div>
      <div className={Style['news-body']}>
        {
          (!data.details || !data.details.length) &&
          (<div style={{ textAlign: 'center', lineHeight: '6rem' }}>无详细内容</div>)
        }
        {
          (data.details && data.details.length) &&
          (<div dangerouslySetInnerHTML={{ __html: delSpam(data.details) }} />)
        }
      </div>
    </article>
  );
};
