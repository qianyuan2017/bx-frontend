import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import XTimelineBasic from './basic';
import Style from './style.less';

// 时间单位
const YEARS = 'years';
const MONTHS = 'months';
const WEEKS = 'weeks';
const DAYS = 'days';
const HOURS = 'hours';
const MINUTES = 'minutes';
const SECONDS = 'seconds';
const MILLISECONDS = 'milliseconds';
const tmUnits = [YEARS, MONTHS, WEEKS, DAYS, HOURS, MINUTES, SECONDS, MILLISECONDS];

// 默认支持模式
const MODE_NORMAL = '';
const MODE_ACTIVE = 'active';
const MODE_INPROCESS = 'inprocess';
const MODE_DISABLE = 'disabled';
const MODE_SELECTED = 'selected';

// 主题样式
const calendarCls = {
  wrap: Style['x-timeline-calendar'],
  active: {
    row: Style['x-timeline-row-active'],
    item: Style['x-timeline-item-active'],
  },
  selected: {
    item: Style['x-timeline-item-selected'],
  },
  disabled: {
    item: Style['x-timeline-item-disabled'],
  },
  inprocess: {
    item: Style['x-timeline-item-inprocess'],
  },
};

// 过滤找到唯一
const oneOf = (dt, filterFunc) => {
  const res = dt.filter(filterFunc);
  return res.length ? res[0] : undefined;
};

// 计算跨行
const crossRows = (start, end, intv, unit) => {
  if (!start || !end || !intv || !unit) {
    return 0;
  }

  // 时间间隔
  const s = moment(start);
  const e = moment(end);
  const d = e.diff(s, unit);
  // 返回指定的间隔与计算间隔的倍数
  return Math.ceil(d / intv);
};

// 是否空对象
const isEmptyObj = (o) => {
  for (const i in o) return false; // eslint-disable-line
  return true;
};

class XTimelineCalendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.genData(this.props),
      selected: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    const data = this.genData(nextProps);
    const selected = [];

    this.setState({
      data,
      selected,
    });
  }

  setSelectedData = (selected) => {
    const data = this.state.data.map((x) => {
      const theOne = oneOf(selected, it => it.extend.no === x.extend.no);
      const mode = theOne ? MODE_SELECTED : (x.mode === MODE_SELECTED ? MODE_NORMAL : x.mode);
      const extend = { ...x.extend, selected: !!theOne };
      return { ...x, mode, extend };
    });
    this.setState({ data, selected });

    // 触发 select 事件
    if (this.props.onSelect) {
      // 只反馈日期字段
      this.props.onSelect(selected.map(x => x.extend.dt));
    }
  }

  handleItemClick = (item) => {
    const { mode, extend: { no } } = item;

    // 如果当前是特殊状态
    if (mode && mode !== MODE_SELECTED) return;

    // 查找对应的 data
    const theOne = oneOf(this.state.data, x => x.extend.no === no);
    this.genSelectdData(theOne);
  }

  genData = (props) => {
    const { timeStart, timeEnd, interval: i, intvUnit: u, dateFormat: f } = props;
    const dtList = props.data || [];
    const limitPast = props.limitPast === undefined ? true : props.limitPast;
    const res = [];
    const now = moment();

    for (let s = moment(timeStart), no = 0; !s.isAfter(timeEnd); s = s.add(i, u), no += 1) {
      const dt = s.format(f);
      const extend = { dt, no, selected: false };

      // 查找匹配数据
      const obj = oneOf(dtList, x => moment(x.start).format(f) === dt) || {};
      const { start, end, ctx } = obj;
      // 如果是跨行的, 计算跨行高度
      const cr = crossRows(start, end, i, u);
      const height = `${cr === 0 ? 1 : cr}00%`;
      const rowStyle = cr > 1 ? { zIndex: '99' } : {};

      const dataMode = isEmptyObj(obj) ? MODE_NORMAL : obj.mode;
      const mode = limitPast && !s.isAfter(now) ? MODE_DISABLE : dataMode;

      const div = Math.floor(1 / i);
      const head = {
        ctx: div < 1 || no % div === 0 ? dt : '',
        node: div < 1 || no % div === 0 ? 'hyphen' : 'none',
      };

      const body = {
        ctx,
        style: { height },
      };


      res.push({ mode, head, body, extend, style: rowStyle });
    }

    return res;
  }

  genSelectdData = (dt) => {
    // 起始选择
    if (this.state.selected.length === 0) {
      if (!dt.mode) {
        const selected = [dt];
        this.setSelectedData(selected);
        return true;
      }

      return false;
    }

    // 已经有选择的情况下，再次选择，形成 range
    // selected 为有序列
    const cur = dt.extend.no;
    const min = this.state.selected[0].extend.no;
    const max = this.state.selected[this.state.selected.length - 1].extend.no;

    // 有一种特殊情况
    // 重复选择了一个节点，即认为是取消选择
    if (cur === min && cur === max) {
      const selected = [];
      this.setSelectedData(selected);
      return true;
    }

    if (cur < min) {
      // 如果是起始之前
      if (!this.validSelectedRange(cur, min)) {
        // 有不能选择的日期节点, 则选择失败
        // console.error('有特殊状态日期节点, 不能选择!')
        return false;
      }

      const selected = this.state.data.filter(x => x.extend.no >= cur && x.extend.no <= max);
      this.setSelectedData(selected);
    } else if (cur > max) {
      // 如果是结束之后
      if (!this.validSelectedRange(max, cur)) {
        // 有不能选择的日期节点, 则选择失败
        // console.error('有特殊状态日期节点, 不能选择!')
        return false;
      }

      const selected = this.state.data.filter(x => x.extend.no >= min && x.extend.no <= cur);
      this.setSelectedData(selected);
    } else {
      // 如果在之间, 那么重新选择
      const selected = [dt];
      this.setSelectedData(selected);
    }

    return true;
  }

  validSelectedRange = (min, max) => {
    // 如果给定区间有特殊的 item, 则失败
    // 节点 mode 非空就说明是特殊的
    for (let no = min; no <= max; no += 1) {
      const theOne = oneOf(this.state.data, x => x.extend.no === no);
      if (theOne && theOne.mode && theOne.mode !== MODE_SELECTED) return false;
    }
    return true;
  }

  render() {
    return (
      <XTimelineBasic
        keyflag={this.props.keyflag || 'calender'}
        data={this.state.data}
        cls={calendarCls}
        lineColor={this.props.lineColor}
        onBodyItemClick={this.handleItemClick}
      />
    );
  }
}

// data 格式
// data = [
//   {
//     mode: '模式',
//     start: '开始时间',
//     end: '结束时间',
//     ctx: '内容 string|React Element',
//   },
// ]

XTimelineCalendar.propTypes = {
  timeStart: PropTypes.instanceOf(Date).isRequired,
  timeEnd: PropTypes.instanceOf(Date).isRequired,
  interval: PropTypes.number.isRequired,
  intvUnit: PropTypes.oneOf(tmUnits).isRequired,
  dateFormat: PropTypes.string.isRequired,

  lineColor: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.object),  // eslint-disable-line
  keyflag: PropTypes.string,
  onSelect: PropTypes.func,
  limitPast: PropTypes.bool,  // eslint-disable-line
};

// 类常量
XTimelineCalendar.YEARS = YEARS;
XTimelineCalendar.MONTHS = MONTHS;
XTimelineCalendar.WEEKS = WEEKS;
XTimelineCalendar.DAYS = DAYS;
XTimelineCalendar.HOURS = HOURS;
XTimelineCalendar.MINUTES = MINUTES;
XTimelineCalendar.SECONDS = SECONDS;
XTimelineCalendar.MILLISECONDS = MILLISECONDS;

XTimelineCalendar.MODE_NORMAL = MODE_NORMAL;
XTimelineCalendar.MODE_ACTIVE = MODE_ACTIVE;
XTimelineCalendar.MODE_DISABLE = MODE_DISABLE;
XTimelineCalendar.MODE_INPROCESS = MODE_INPROCESS;
// XTimelineCalendar.MODE_SELECTED = MODE_SELECTED;

export default XTimelineCalendar;
