import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Style from './style.less';

class XTimelineBasic extends React.Component {
  handleHeadItemClick = item => () => {
    return this.props.onHeadItemClick && this.props.onHeadItemClick(item);
  }

  handleBodyItemClick = item => () => {
    return this.props.onBodyItemClick && this.props.onBodyItemClick(item);
  }

  render() {
    // 线的颜色
    const lineColor = this.props.lineColor ? { borderColor: this.props.lineColor } : {};

    // 自定义 class
    const propsCls = this.props.cls || {};
    const wrapCls = classNames(Style['x-timeline'], propsCls.wrap);

    //
    const keyflag = this.props.keyflag;
    const renderData = this.props.data || [];

    const renderRow = renderData.map((it, inx) => {
      const { style, head: h, body: b, mode } = it;
      const { ctx: hCtx, node, style: hStyle } = h || {};
      const { ctx: bCtx, style: bStyle } = b || {};
      const { row: rowCls, icon: iconCls, head: headCls, body: bodyCls, item: itemCls } = propsCls[`${mode}`] || {};

      const nodeCls = node || 'default';
      const genKey = pre => `xtml-${keyflag}-${pre}-item${inx}`;

      const newHead = (
        <div
          key={genKey('head')}
          className={classNames(Style['x-timeline-head'], headCls)}
          style={lineColor}
        >
          <div
            className={classNames(Style['x-timeline-item'], itemCls)}
            style={hStyle}
            onClick={this.handleHeadItemClick(it)}
          >{hCtx}</div>
          {
            nodeCls !== 'none' &&
            (
              <div className={Style['x-timeline-icon']}>
                {
                  nodeCls instanceof Function ?
                  nodeCls() :
                  (
                    <div className={classNames(Style[`x-timeline-icon-${nodeCls}`], iconCls)} />
                  )
                }
              </div>
            )
          }
        </div>
      );

      const newBody = (
        <div
          key={genKey('body')}
          className={classNames(Style['x-timeline-body'], bodyCls)}
        >
          <div
            className={classNames(Style['x-timeline-item'], itemCls)}
            style={bStyle}
            onClick={this.handleBodyItemClick(it)}
          >{bCtx}</div>
        </div>
      );

      return (
        <div
          key={genKey('row')}
          style={style}
          className={classNames(Style['x-timeline-row'], rowCls)}
        >
          {newHead}
          {newBody}
        </div>
      );
    });

    return (
      <div className={wrapCls}>
        {renderRow}
      </div>
    );
  }
}

// data 格式
// data = [
//   {
//     mode: '节点类型 string',
//     style: '当前行 style',
//     head: {
//       ctx: '内容 string|React Element',
//       node: '类型, 目前有 default|point|hyphen|none',
//       style: '样式表 object',
//     },
//     body: {
//       ctx: '内容 string|React Element',
//       style: '样式表 object',
//     },
//   },
// ]

// cls 自定义样式 class
// 这个允许用户有极大的自由, 可以封装几乎是任意的类
// cls 只有一个属性要求, 就是 wrap 属性，定义了最外层的 class
// 如果数据分为 disable, normal, acive 三个 mode
// 那么 cls 格式为
// cls = {
//   wrap: 'warp-timeline',
//   diable: {
//     row: 'row-class',
//     icon: 'icon-class',
//     head: 'head-class',
//     body: 'body-class',
//     item: 'item-class',
//   },
//   normal: xxxx,
//   active: xxxx,
// }

XTimelineBasic.propTypes = {
  keyflag: PropTypes.string.isRequired,
  lineColor: PropTypes.string,
  cls: PropTypes.object,
  data: PropTypes.arrayOf(PropTypes.object),
  onBodyItemClick: PropTypes.func,
  onHeadItemClick: PropTypes.func,
};

export default XTimelineBasic;
