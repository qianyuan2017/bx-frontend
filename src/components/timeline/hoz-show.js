import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import Style from './style.less';

// 时间单位
const YEARS = 'years';
const MONTHS = 'months';
const WEEKS = 'weeks';
const DAYS = 'days';
const HOURS = 'hours';
const MINUTES = 'minutes';
const SECONDS = 'seconds';
const MILLISECONDS = 'milliseconds';
const tmUnits = [YEARS, MONTHS, WEEKS, DAYS, HOURS, MINUTES, SECONDS, MILLISECONDS];

// 默认支持模式
const MODE_NORMAL = '';
const MODE_ACTIVE = 'active';

// 过滤找到唯一
const oneOf = (dt, filterFunc) => {
  const res = dt.filter(filterFunc);
  return res.length ? res[0] : undefined;
};

class HozShow extends React.Component {
  constructor(props) {
    super(props);

    const calList = this.genColumns(this.props);
    this.state = {
      calList,
      data: this.genData(this.props.data, calList),
    };
  }

  componentWillReceiveProps(nextProps) {
    const calList = this.genColumns(nextProps);
    const data = this.genData(nextProps.data, calList);
    this.setState({
      calList,
      data,
    });
  }

  handleRowClick = record => () => {
    if (this.props.onRowClick) {
      this.props.onRowClick(record.extend);
    }
  }

  genColumns = (props) => {
    const { timeStart, timeEnd, interval: i, intvUnit: u } = props;
    const res = [];

    for (let s = moment(timeStart), no = 0; !s.isAfter(timeEnd); s = s.add(i, u), no += 1) {
      res.push({ dt: moment(s), mode: MODE_NORMAL, no });
    }

    return res;
  }

  genData = (propsData, calList) => {
    const dtList = propsData || [];

    const data = dtList.map((dt) => {
      const { ctx: head, list } = dt;
      const oriList = list || [];

      const bdList = calList.map((it) => {
        const theOne = oneOf(oriList, (x) => {
          const { start, end } = x;
          return !moment(start).isAfter(it.dt) && moment(end).isAfter(it.dt);
        });

        return { ...it, mode: theOne ? MODE_ACTIVE : MODE_NORMAL };
      });

      return { head, body: bdList, extend: dt };
    });

    return data;
  }

  render() {
    const keyGen = (f, k) => `xtml-${f}-${k}`;
    const renderItem = (it, inx, keyFlag) => {
      const itemExtCls = it.mode ? Style[`x-timeline-hozshow-item-${it.mode}`] : '';
      return (
        <div
          key={keyGen(keyFlag, `item${inx}`)}
          className={classNames(Style['x-timeline-hozshow-item'], itemExtCls)}
        />
      );
    };

    const renderRow = (dt, inx, keyFlag) => {
      return (
        <div
          key={keyGen(keyFlag, `row${inx}`)}
          className={Style['x-timeline-hozshow-row']}
          onClick={this.handleRowClick(dt)}
        >
          <div className={Style['x-timeline-hozshow-head']}>
            <span>
              {dt.head}
            </span>
          </div>
          <div className={Style['x-timeline-hozshow-body']}>
            {dt.body && dt.body.map((x, i) => renderItem(x, i, `${keyFlag}-row${i}`))}
          </div>
        </div>
      );
    };

    const renderTitleColumn = (titleSpan, titleShow) => {
      const { timeStart, timeEnd } = this.props;
      const intv = moment(timeEnd).diff(timeStart) / (titleSpan + 1);

      const cols = [];
      const s = moment(timeStart);
      const e = moment(timeEnd);

      // 显示 titleSpan 比如 4个, 但是需要求值 5 次
      for (let i = 0; i < titleSpan; i += 1) {
        const t = s.add(intv * i, MILLISECONDS);
        const show = (titleShow instanceof Function) ? titleShow(t.toDate()) : t.format(titleShow);
        const lastShow = i !== titleSpan - 1 ? false : (
          <div className={Style['x-timeline-hozshow-ctx-tail']}>
            {(titleShow instanceof Function) ? titleShow(e.toDate()) : e.format(titleShow)}
          </div>
        );

        cols.push(
          <div key={keyGen('title', `col${i}`)} className={Style['x-timeline-hozshow-guide']}>
            <div className={Style['x-timeline-hozshow-ctx']}>{show}{lastShow}</div>
            <div className={Style['x-timeline-hozshow-icon']} />
          </div>,
        );
      }

      return cols;
    };

    const renderTitle = () => {
      const { titleSpan, titleShow } = this.props;

      // 默认 4 个
      const span = Math.abs(titleSpan) || 4;
      if (!titleShow) {
        return false;
      }

      return (
        <div className={classNames(Style['x-timeline-hozshow-title'])}>
          <div className={Style['x-timeline-hozshow-head']}>
            <div className={Style['x-timeline-hozshow-ctx']} />
          </div>
          <div className={Style['x-timeline-hozshow-body']} >
            {renderTitleColumn(span, titleShow)}
          </div>
        </div>
      );
    };

    return (
      <div className={Style['x-timeline-hozshow']}>
        {renderTitle()}
        {this.state.data && this.state.data.map((x, i) => renderRow(x, i, 'hozshow'))}
      </div>
    );
  }
}

// data 格式
// data = [
//   {
//     ctx: '内容 string|React Element',
//     list: [
//       {
//          start: '开始时间',
//          end: '结束时间',
//       },
//     ],
//   },
// ]

HozShow.propTypes = {
  timeStart: PropTypes.instanceOf(Date).isRequired,
  timeEnd: PropTypes.instanceOf(Date).isRequired,
  interval: PropTypes.number.isRequired,
  intvUnit: PropTypes.oneOf(tmUnits).isRequired,
  data: PropTypes.arrayOf(PropTypes.object),
  onRowClick: PropTypes.func,
  titleSpan: PropTypes.number,
  titleShow: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.string,
  ]),
};

// 类常量
HozShow.YEARS = YEARS;
HozShow.MONTHS = MONTHS;
HozShow.WEEKS = WEEKS;
HozShow.DAYS = DAYS;
HozShow.HOURS = HOURS;
HozShow.MINUTES = MINUTES;
HozShow.SECONDS = SECONDS;
HozShow.MILLISECONDS = MILLISECONDS;

export default HozShow;
