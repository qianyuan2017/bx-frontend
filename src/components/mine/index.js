import React from 'react';
import { List } from 'antd-mobile';
import Avatar from '../common/avatar';

const Item = List.Item;

export default (props) => {
  const { avatar, title: avatarTip } = props.avatar || {};

  return (
    <div>
      <header style={{ textAlign: 'center', padding: '64px' }}>
        <div>
          <Avatar width="128px" image={avatar} />
        </div>
        <div>
          {avatarTip || 'Hi 你好!'}
        </div>
      </header>
      <section>
        <List>
          {
            (props.list || []).map((it, inx) => {
              const { thumb, title, extra, onClick, arrow: arr } = it;
              const arrow = arr === null ? 'empty' : arr || 'horizontal';

              return (
                <Item
                  key={`mine-item-${inx}`}
                  thumb={thumb}
                  extra={extra}
                  onClick={onClick}
                  arrow={arrow}
                >{title}</Item>
              );
            })
          }
        </List>
      </section>
    </div>
  );
};
