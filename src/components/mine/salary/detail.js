import React from 'react';
import { List, WhiteSpace } from 'antd-mobile';
import lodash from 'lodash';
import { cutByVisual } from '../../../utils/strings';

// 分隔符为4个空格
const separator = '    ';

export default (props) => {
  const {
    summary,
    billItemNames1,
    billItemValues1,
    billItemNames2,
    billItemValues2,
    billItemNames3,
    billItemValues3,
    billItemNames4,
    billItemValues4,
  } = props.data || {};

  const summaries = (summary || '').split(separator).reduce((acc, val, inx) => {
    if (inx % 2 === 0) {
      const item = {
        title: val.trim(),
        extra: '',
      };
      return [...acc, item];
    } else {
      const last = acc[acc.length - 1];
      last.extra = val.trim();
      return [...acc.filter(x => x.title !== last.title), last];
    }
  }, []).filter(x => !!x.title);

  const details = lodash.sortBy(
    [
      ...cutByVisual(billItemNames1, billItemValues1),
      ...cutByVisual(billItemNames2, billItemValues2),
      ...cutByVisual(billItemNames3, billItemValues3),
      ...cutByVisual(billItemNames4, billItemValues4),
    ],
    x => (x.extra ? 0 : 1),
  );

  return (
    <div>
      <List renderHeader={() => '概览'}>
        {
          summaries.map((s, i) => {
            return (
              <List.Item key={`si-${i}`} extra={s.extra}>{s.title}</List.Item>
            );
          })
        }
      </List>
      <WhiteSpace size="lg" />
      <List renderHeader={() => '明细'}>
        {
          details.map((s, i) => {
            return (
              <List.Item key={`si-${i}`} extra={s.extra}>{s.title}</List.Item>
            );
          })
        }
      </List>
    </div>
  );
};
