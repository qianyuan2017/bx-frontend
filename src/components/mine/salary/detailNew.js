import React from 'react';
import { List } from 'antd-mobile';
import { getSalaryItems } from './strings';

export default (props) => {
  const {
    billItemNames1,
    billItemValues1,
    billItemNames2,
    billItemValues2,
    billItemNames3,
    billItemValues3,
  } = props.data || {};

  const details = [
    ...getSalaryItems(billItemNames1, billItemValues1, 1),
    ...getSalaryItems(billItemNames2, billItemValues2, 2),
    ...getSalaryItems(billItemNames3, billItemValues3, 3),
  ];

  return (
    <div>
      <List>
        {
          details.map((s, i) => {
            return (
              <List.Item key={`si-${i}`} extra={s.extra}>{s.title}</List.Item>
            );
          })
        }
      </List>
    </div>
  );
};
