import React from 'react';
import { List } from 'antd-mobile';

export default (props) => {
  const handleItemClick = it => () => {
    if (props.onItemClick) {
      props.onItemClick(it);
    }
  };

  const renderList = () => {
    if (!props.data || !props.data.length) return undefined;
    return props.data.map((it) => {
      return (
        <List.Item
          key={`${it.empid}-${it.prvdit}`}
          extra={`${it.rdlamt}`}
          arrow="horizontal"
          onClick={handleItemClick(it)}
        >{it.prvdit}</List.Item>
      );
    });
  };

  return (
    <div>
      <List>
        {renderList()}
      </List>
    </div>
  );
};
