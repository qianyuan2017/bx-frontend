
// 拆分工资名称与金额
// 起始位置  迄止位置  文字
// 1         8       岗位工资
// 10        14      绩效
// 16        24      厂龄补贴
// 26        34      其他补贴
// 36        42      电话费
// 44        50      夜班费
// 52        60      安全考核
// 62        70      政务补贴
// 72        86      业务消防员津贴

// 起始位置  迄止位置  文字
// 1         8       降温费
// 9         19      补贴与奖励
// 21        29      职务津贴
// 31        37      加班费
// 39        51      年度先进奖励
// 53        57      补发
// 59        67      扣发工资
// 69        77      其他扣款
// 79        87      补扣社保

// 起始位置  迄止位置  文字
// 1         8       工会会费
// 10        18      大病统筹
// 20        28      扣养老金
// 30        38      扣医保费
// 40        48      扣失业金
// 50        58      扣公积金
// 60        66      代扣税
// 68        76      应发合计
// 78        86      扣款合计
// 88        96      实发工资

// 对应上表的位置
const posList = [
  [1, 10, 16, 26, 36, 44, 52, 62, 72],
  [1, 9, 21, 31, 39, 53, 59, 69, 79],
  [1, 10, 20, 30, 40, 50, 60, 68, 78, 88],
];

export function getSalaryItems(names, values, index) {
  if (index > 3) return [];
  if (!names || !names.length) return [];

  // 找到对应的位置数据
  const pos = posList[index - 1];
  // 按空格拆分, 并把无效内容去掉
  const nameArr = names.split(' ').filter(Boolean);
  //
  if (pos.length !== nameArr.length) {
    console.error('拆分工资数据失败');
    return [];
  }

  // 数据则按照位置拆分
  const valueArr = values.split('');

  return nameArr.map((key, inx) => {
    // pos 数组的位置是从1开始的
    // 转换为数组下标, 则需要 -1
    const start = pos[inx];
    const end = pos[inx + 1];

    const val = valueArr.slice(start, end).join('').trim();

    return {
      title: key,
      extra: val,
    };
  });
}
