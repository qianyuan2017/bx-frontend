import React from 'react';
import { Router } from 'dva/router';
import getRoutes from './routes';

export default ({ history, app }) => {
  const routes = getRoutes(app);
  return (
    <Router history={history} routes={routes} />
  );
};
