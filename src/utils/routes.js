const routePrefix = '/m';

export default {
  index: `${routePrefix}/`,
  company: {
    news: {
      list: `${routePrefix}/company/news`,
      detail: `${routePrefix}/company/news`,
    },
  },
  mine: {
    salary: {
      list: `${routePrefix}/mine/salary`,
      detail: `${routePrefix}/mine/salary`,
    },
    index: `${routePrefix}/mine`,
    login: `${routePrefix}/mine/login`,
    timebook: `${routePrefix}/mine/timebook`,
    getPassword: `${routePrefix}/mine/getpassword`,
  },
  meetingroom: {
    meeting: {
      list: `${routePrefix}/meetingroom/list`,
      mymeeting: `${routePrefix}/meetingroom/mymeetings`,
      preelection: `${routePrefix}/meetingroom/preelection`,
      apply: `${routePrefix}/meetingroom/apply`,
      detail: `${routePrefix}/meetingroom/detail`,
      approval: `${routePrefix}/meetingroom/approval`,
    },
  },
};
