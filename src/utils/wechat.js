import _ from 'lodash';
// import cookies from 'cookiesjs';

const wxCropId = 'wx1627d16af9facfcb';
const oauthUrl = () => 'https://open.weixin.qq.com/connect/oauth2/authorize';
const state = () => 'abc123';

// export const validWechat = () => {
//   const empid = cookies('wechat.empid');
//   if (!empid) {
//     const hasCode = /code=[^&/#]+/i;
//     if (!hasCode.exec(window.location.href)) {
//       redirectToWx();
//     }
//   }
// };

export const oauthRequest = (uri, corpId) => {
  const path = oauthUrl();
  const hash = 'wechat_redirect';
  const params = {
    appid: corpId,
    redirect_uri: window.encodeURIComponent(uri),
    response_type: 'code',
    scope: 'snsapi_privateinfo',
    agentid: 1000002,
    state: state(),
  };

//   const paramStr = _.transform(params, (r, v, k) => `${r}&${k}=${v}`, '');
  let paramStr = '';
  _.transform(params, (r, v, k) => {
    paramStr = `${paramStr}&${k}=${v}`;
  }, '');

  paramStr = _.trimStart(paramStr, '&');

  return `${path}?${paramStr}#${hash}`;
};

export const redirectToWx = () => {
  const uri = window.location.href
    .replace(/code=[^&/#]*/, '')  // 去掉原来的 code
    .replace(/state=[^&/#]*/, '')  // 去掉原来的 state
    .replace('?&', '?');

  window.location.href = oauthRequest(uri, wxCropId);
};

export const getWechatCode = (dispatch, location) => {
  if (process.env.NODE_ENV === 'development') return true;

  if (location.query.code) {
    // 如果 url search 中含有 code, 说明是微信跳转过来的
    const code = location.query.code;
    const now = (new Date()).valueOf();
    const expire = now + 7200;

    window.sessionStorage.setItem('auth2.code', code);
    window.sessionStorage.setItem('auth2.expire', expire);

    dispatch({ type: 'app/syncState', payload: { code } });
  } else {
    // 如果没有，查找本地缓存
    const code = window.sessionStorage.getItem('auth2.code');
    const expire = window.sessionStorage.getItem('auth2.expire') - 0;
    const now = (new Date()).valueOf();

    if (!code || now > expire) {
      redirectToWx();
      return false;
    }

    dispatch({ type: 'app/syncState', payload: { code } });
  }

  return true;
};
