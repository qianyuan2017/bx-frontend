// 框架相关
export const RESPONSE_SUCCESS = 0;

// 业务相关
export const STATUS = {
  ready: '0',
  agree: '1',
  disAgree: '2',
  cancel: '3',
};
