import { routerRedux } from 'dva/router';
import icons from './icons';
import route from './routes';

export default {
  app: {
    items: [
      {
        title: '公司',
        key: 'company',
        icon: { uri: icons.company.normal },
        selectedIcon: { uri: icons.company.selected },
        onPress: (dispatch) => {
          dispatch(routerRedux.push(route.company.news.list));
        },
      },
      {
        title: '活动',
        key: 'activity',
        icon: { uri: icons.activity.normal },
        selectedIcon: { uri: icons.activity.selected },
        onPress: (dispatch) => {
          dispatch(routerRedux.push(route.activity));
        },
      },
      {
        title: '会议室',
        key: 'meetingroom',
        icon: { uri: icons.meetingroom.normal },
        selectedIcon: { uri: icons.meetingroom.selected },
        onPress: (dispatch) => {
          dispatch(routerRedux.push(route.meetingroom.meeting.list));
        },
      },
      {
        title: '我的',
        key: 'mine',
        icon: { uri: icons.mine.normal },
        selectedIcon: { uri: icons.mine.selected },
        onPress: (dispatch) => {
          dispatch(routerRedux.push(route.mine.index));
        },
      },
    ],
  },
};