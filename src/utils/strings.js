
// s1, s2 在视觉上有对应关系
// 截取对应的字串
export const cutByVisual = (s1, s2) => {
  const separator = ' '; // 分隔符为 1 个空格
  const hanLen = 2; // 1 个汉字占 2 空格

  // a1, a2 记录对应的字串及位置
  const a1 = getSplitedStrBySep(s1, separator);
  const a2 = getSplitedStrBySep(s2, separator);

  // 以 a1 的位置为主导, 对 a2 过滤
  let dev = 0; // 大致的总偏移 或者 模糊位置
  const rng = 2 * hanLen; // 当前位置区间的偏移

  const res = [];
  a1.forEach(({ val, start, end }) => {
    const s = start + dev - rng; // eslint-disable-line
    const e = end + dev + rng;
    const r = {
      title: val,
      extra: '',
    };

    const t = a2.filter(x => x.start >= s && x.start <= e)[0];
    if (t) {
      r.extra = t.val;
      dev += (val.length * hanLen) - t.val.length;
    } else {
      dev += val.length * hanLen;
    }

    res.push(r);
  });

  return res;
};

const getSplitedStrBySep = (s, sp) => {
  const res = [];
  if (!s) return res;

  let k = {
    val: '',  // 对应的值
    start: 0, // 起始位置
    end: 0, // 结束位置
  };

  for (let i = 0; i < s.length; i += 1) {
    if (s[i] === sp) {
      // 当前是分隔符, 代表开始或者结束
      k = {
        val: '',
        start: i,
        end: i,
      };
      continue; // eslint-disable-line
    }

    if (k.val !== '') {
      // 如果上次的字符是有的, 说明这个单词可能还没结束
      k.val += s[i];
      k.end = i;

      // 弹出最新的记录, 因为当前的 k 才是最新正确的
      res.pop();
    } else {
      // 否则意味着新单词开始
      k.val = s[i];
      k.start = i;
      k.end = i;
    }
    // k 被认为是正确的
    res.push(k);
  }

  return res;
};
