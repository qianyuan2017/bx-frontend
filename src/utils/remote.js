const apiPrefix = '/api';

export default {
  app: {
    user: `${apiPrefix}/empid/empid`,
    getPassword: `${apiPrefix}/news/pwd`,
  },
  company: {
    news: {
      listOnLoad: `${apiPrefix}/news/news`,
    },
  },
  mine: {
    salary: {
      listOnLoad: `${apiPrefix}/salary/salaryInfo`,
      detailOnLoad: `${apiPrefix}/salary/salaryBill`,
    },
    timebook: {
      list: `${apiPrefix}/salary/timebook`,
    },
  },
  meetingroom: {
    rooms: `${apiPrefix}/meeting/meetingRooms`,
    myapply: `${apiPrefix}/meeting/appointApplyPage`,
    applyToMe: `${apiPrefix}/meeting/meetingManagePage`,
    roomWeekSchedule: `${apiPrefix}/meeting/roomWeekSchedule`,
    users: `${apiPrefix}/meeting/deptPersons`,
    apply: `${apiPrefix}/meeting/appointApply`,
    cancel: `${apiPrefix}/meeting/meetingCancel`,
    approval: `${apiPrefix}/meeting/appointApproval`,
    delApply: `${apiPrefix}/meeting/delApply`,
  },
};
