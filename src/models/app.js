import modelExtend from 'dva-model-extend';
import pathToRegexp from 'path-to-regexp';
import base from './base';
import route from '../utils/routes';
import api from '../utils/remote.js';
import { get } from '../utils/services';
import tabSetting from '../utils/tabbar';
import { redirectToWx } from '../utils/wechat';

const { company, mine, meetingroom } = route;
const { news } = company;
const { salary } = mine;
const { meeting } = meetingroom;

export default modelExtend(base, {
  namespace: 'app',

  state: {
    user: {
      empid: process.env.NODE_ENV === 'development' ? 'L000001160' : window.cookies('wechat.empid'),
      password: '',
    },
    code: '',
    depts: [],
    tabbar: {
      hidden: false,
      active: '',
      items: tabSetting.app.items,
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        // 微信 auth2 code
        // if (!getWechatCode(dispatch, location)) {
        //   return undefined;
        // }
        if (process.env.NODE_ENV !== 'development') {
          dispatch({ type: 'getWechatCode', payload: { location } });
        }

        let active;
        switch (location.pathname) {
          case news.list:
            active = 'company';
            break;
          case mine.index:
          case salary.list:
          case meeting.mymeeting:
            active = 'mine';
            break;
          case meeting.list:
          case meeting.apply:
          case pathToRegexp(`${meeting.preelection}/:m+`).exec(location.pathname):
            active = 'meetingroom';
            break;
          default:
        }

        if (active) {
          dispatch({ type: 'setTabBar', payload: { hidden: false, active } });
        } else {
          dispatch({ type: 'setTabBar', payload: { hidden: true } });
        }
      });
    },
  },

  effects: {
    *getWechatCode({ payload }, { call, put, select }) {
      const empid = yield select(s => s.app.user.empid);
      if (empid) return undefined;

      const location = payload.location;
      const code = location.query.code;
      if (!code) {
        redirectToWx();
      } else {
        const empidDt = yield call(get, api.app.user, { code });

        if (!empidDt || !empidDt.data) {
          throw new Error('应用初始化失败, 请退出重试');
        }

        const { success, msg } = empidDt.data;
        if (!success) {
          throw new Error(`获取人员信息失败: ${msg}`);
        }

        yield put({ type: 'syncState', payload: { code } });
        yield put({ type: 'updateUser', payload: { empid: msg } });
        window.cookies({ 'wechat.empid': msg });
      }
    },

    *getPassword({ payload }, { put, call, select }) {
      const empid = yield select(s => s.app.user.empid);
      const res = yield call(get, api.app.getPassword, { empid });

      if (!res || !res.data) {
        throw new Error('找回密码失败, 请退出重试');
      }

      if (!res.data.success) {
        throw new Error(res.data.msg);
      }

      const password = res.data.msg;
      yield put({ type: 'updateUser', payload: { password } });
    },

    *appInit({ payload }, { put, call }) {
      const [deptDt] = yield [
        call(get, api.meetingroom.users),
      ];

      if (!deptDt || !deptDt.data) {
        throw new Error('应用初始化失败, 请退出重试');
      }

      const depts = deptDt.data;
      yield put({ type: 'syncState', payload: { depts } });
    },

    *setTabBar({ payload }, { put, select }) {
      const oriTabbar = yield select(s => s.app.tabbar);
      const tabbar = { ...oriTabbar, ...payload };
      yield put({ type: 'syncState', payload: { tabbar } });
    },
  },

  reducers: {
    updateUser(state, { payload }) {
      const user = { ...state.user, ...payload };
      return { ...state, user };
    },
  },
});
