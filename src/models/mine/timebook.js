import modelExtend from 'dva-model-extend';
import moment from 'moment';
import { routerRedux } from 'dva/router';
import base from '../base';
import { get } from '../../utils/services';
import { mine } from '../../utils/remote';
import routes from '../../utils/routes';

export default modelExtend(base, {
  namespace: 'timebook',

  state: {},

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === routes.mine.timebook) {
          dispatch({ type: 'list', payload: { location } });
          return undefined;
        }
      });
    },
  },

  effects: {
    *list({ payload }, { call, put, select }) {
      // const empid = yield select(s => s.app.user.empid);
      // if (!empid) {
      //   yield put(routerRedux.push(routes.mine.login));
      //   return undefined;
      // }

      const empid = yield select(s => s.app.user.empid);
      if (!empid) {
        yield put({ type: 'getWechatCode', payload: { location: (payload || {}).location } });
      }

      const mon = (payload || {}).month || moment().format('YYYY-MM');
      const fristDay = `${mon}-01`.replace(/-/g, '');
      const lastDay = moment(mon).add(1, 'month').add(-1, 'day').format('YYYYMMDD');

      const res = yield call(get, mine.timebook.list, { empid, beginDate: fristDay, endDate: lastDay });
      if (!res || res.err) {
        console.log(res)
        throw new Error('远程请求超时或者有错误');
      }

      yield put({ type: 'syncState', payload: { list: res.data } });
    },
  },

  reducers: {},
});
