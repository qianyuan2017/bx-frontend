import modelExtend from 'dva-model-extend';
import { routerRedux } from 'dva/router';
import base from '../base';
import icons from '../../utils/icons';
import routes from '../../utils/routes';

export default modelExtend(base, {
  namespace: 'mine',

  state: {
    mine: {
      avatar: '',
      title: '',
    },
    list: [
      {
        title: '工资条',
        link: routes.mine.salary.list,
        thumb: icons.salary.selected,
      },
      {
        title: '会议室',
        link: routes.meetingroom.meeting.mymeeting,
        thumb: icons.meetingroom.selected,
      },
      {
        title: '考勤表',
        link: routes.mine.timebook,
        thumb: icons.mine.timebook,
      },
      {
        title: '找回密码',
        link: routes.mine.getPassword,
        thumb: icons.mine.getPassword,
      },
    ],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === routes.mine.index) {
          dispatch({ type: 'onLoad', payload: { location } });
          return undefined;
        }

        if (location.pathname === routes.mine.getPassword) {
          dispatch({ type: 'app/getPassword' });
          return undefined;
        }
      });
    },
  },

  effects: {
    *onLoad({ payload }, { call, put, select }) {  // eslint-disable-line
      // const empid = yield select(s => s.app.user.empid);

      // if (!empid) {
      //   yield put(routerRedux.push(routes.mine.login));
      //   return undefined;
      // }

      const code = yield select(s => s.app.code);
      if (!code) {
        yield put({ type: 'getWechatCode', payload });
      }
    },
  },

  reducers: {},
});
