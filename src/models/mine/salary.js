import modelExtend from 'dva-model-extend';
import { routerRedux } from 'dva/router';
import pathToRegexp from 'path-to-regexp';
import lodash from 'lodash';
import base from '../base';
import { get } from '../../utils/services';
import { mine } from '../../utils/remote';
import route from '../../utils/routes';

export default modelExtend(base, {
  namespace: 'salary',

  state: {},

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === route.mine.salary.list) {
          dispatch({ type: 'listOnLoad', payload: { location } });
          return undefined;
        }

        //
        const m1 = pathToRegexp(`${route.mine.salary.detail}/:dt`).exec(location.pathname);
        if (m1) {
          dispatch({ type: 'detailOnLoad', payload: { checkDate: m1[1], location } });
          return undefined;
        }
      });
    },
  },

  effects: {
    *listOnLoad({ payload }, { call, put, select }) {  // eslint-disable-line
      // const empid = yield select(s => s.app.user.empid);
      // if (!empid) {
      //   yield put(routerRedux.push(route.mine.login));
      //   return undefined;
      // }

      const empid = yield select(s => s.app.user.empid);
      if (!empid) {
        yield put({ type: 'getWechatCode', payload });
      }

      const thisYear = (new Date()).getFullYear();
      const lastYear = thisYear - 1;

      const [res1, res2] = yield [
        call(get, mine.salary.listOnLoad, { empid, year: thisYear }),
        call(get, mine.salary.listOnLoad, { empid, year: lastYear }),
      ];

      if (!res1 || !res2) {
        throw new Error('远程请求超时或者有错误');
      }

      const list = lodash.sortBy(
        [
          ...res1.data,
          ...res2.data,
        ],
        [x => 0 - x.prvdit],
      );

      if (list.length === 0) {
        throw new Error('抱歉, 没有查询到数据!');
      }

      yield put({
        type: 'syncState',
        payload: { list },
      });
    },

    *detailOnLoad({ payload }, { call, put, select }) {  // eslint-disable-line
      // const empid = yield select(s => s.app.user.empid);

      // if (!empid) {
      //   yield put(routerRedux.push(route.mine.login));
      //   return undefined;
      // }

      const empid = yield select(s => s.app.user.empid);
      if (!empid) {
        yield put({ type: 'getWechatCode', payload });
      }

      const theDate = (payload || {}).checkDate;

      const dt = yield call(get, mine.salary.detailOnLoad, { empid, prvdit: theDate });
      const res = dt.data;
      if (!res) {
        throw new Error('抱歉, 没有查询到数据!');
      }

      yield put({
        type: 'syncState',
        payload: { detail: res },
      });
    },
  },

  reducers: {
    syncCheckDate(state, { payload }) {
      const { data, type: typ } = payload;
      const list = state.list.filter(x => x.type !== typ);
      return { ...state, list: [...list, { type: typ, data }] };
    },
  },
});
