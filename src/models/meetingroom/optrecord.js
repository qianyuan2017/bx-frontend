import modelExtend from 'dva-model-extend';
import moment from 'moment';
import lodash from 'lodash';
import pathToRegexp from 'path-to-regexp';
import { routerRedux } from 'dva/router';
import { get, post } from '../../utils/services';
import routes from '../../utils/routes';
import { meetingroom } from '../../utils/remote.js';
import base from '../base';

export default modelExtend(base, {
  namespace: 'optrecord',

  state: {
    date: moment(new Date()),
    myApplies: [],
    otherApplies: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        // 列表页
        if (location.pathname === routes.meetingroom.meeting.list) {
          dispatch({ type: 'getSchedulesByWeek' });
          return undefined;
        }

        // 我的会议
        if (location.pathname === routes.meetingroom.meeting.mymeeting) {
          dispatch({ type: 'myMeetingOnLoad' });
          return undefined;
        }

        // 会议-详情
        const m2 = pathToRegexp(`${routes.meetingroom.meeting.detail}/:appointId`).exec(location.pathname);
        if (m2) {
          dispatch({ type: 'meetingPageOnLoad', payload: { appointId: m2[1] } });
          return undefined;
        }

        // 会议-审批
        const m3 = pathToRegexp(`${routes.meetingroom.meeting.approval}/:appointId`).exec(location.pathname);
        if (m3) {
          const appointId = m3[1];
          const { empid, roomName, beginDate, endDate } = location.query;

          dispatch({
            type: 'meetingApprovalOnLoad',
            payload: {
              empid,
              appointId,
              roomName,
              beginDate,
              endDate,
            },
          });

          return undefined;
        }
      });
    },
  },

  effects: {
    *getSchedulesByWeek({ payload }, { call, put, select }) {  // eslint-disable-line
      const [resSches, resRooms] = yield [
        call(get, meetingroom.roomWeekSchedule, payload),
        call(get, meetingroom.rooms, payload),
      ];
      if (!resSches || !resSches.data) {
        throw new Error('请求会议预定列表失败, 远程请求可能不正确');
      }
      if (!resRooms || !resRooms.data) {
        throw new Error('请求会议室列表失败, 远程请求可能不正确');
      }

      const { weekdays, schedules, nextMonday } = resSches.data;

      const list = (schedules || []).reduce((acc, it) => {
        const { room, daySchedules: dsc } = it;

        const schList = dsc.reduce((a, x) => {
          const { day, appoints } = x;

          if (a[`${day}`]) {
            const newRec = { ...a[`${day}`], [`${room.roomCode}`]: appoints };
            return { ...a, [`${day}`]: newRec };
          }

          return { ...a, [`${day}`]: { [`${room.roomCode}`]: appoints } };
        }, {});

        const newAcc = { ...acc };
        lodash.forEach(schList, (sch, day) => {
          newAcc[day] = { ...(newAcc[day] || {}), ...sch };
        });

        return { ...newAcc };
      }, {});

      const rooms = (resRooms.data || []).reduce((acc, it) => {
        return { ...acc, [`${it.roomCode}`]: it };
      }, {});

      if (!payload) {
        yield put({ type: 'syncState', payload: { weekdays, list, rooms, nextMonday } });
      } else {
        yield put({ type: 'appendSchedules', payload: { weekdays, list, rooms, nextMonday } });
      }
    },

    *applyMeetingRoom({ payload }, { call, put }) {  // eslint-disable-line
      const dt = yield call(post, meetingroom.apply, payload);
      const res = dt.data;
      if (!res) {
        console.log(dt);
        throw new Error('申请失败, 原因未知, 远程请求可能不正确');
      }

      // // 成功之后跳转-详情
      // const redirectTo = `${route.meeting}/${res.message.Resource.RecId}`;
      // yield put(routerRedux.push(redirectTo));
      yield put({
        type: 'alert',
        payload: {
          title: '申请成功',
          message: '您的会议室申请提交成功, 请等待审核',
        },
      });
    },
    *myMeetingOnLoad({ payload }, { call, put, select }) {
      const empid = yield select(s => s.app.user.empid);
      const queryParams = {
        pageNo: 1,
        pageSize: 99,
        empid,
      };

      const [res1, res2] = yield [
        call(get, meetingroom.myapply, queryParams),
        call(get, meetingroom.applyToMe, queryParams),
      ];

      const fromMe = res1.data;
      const toMe = res2.data;
      if (!fromMe || !toMe) {
        // console.log(dt);
        throw new Error('查询我的申请列表失败, 远程请求可能不正确');
      }

      const myApplies = fromMe.results || [];
      const otherApplies = toMe.results || [];

      yield put({ type: 'syncState', payload: { myApplies, otherApplies } });
    },

    *meetingPageOnLoad({ payload }, { put }) {
      yield put({ type: 'syncDetail', payload });
    },

    *meetingApprovalOnLoad({ payload }, { call, put }) {
      const queryParams = {
        pageNo: 1,
        pageSize: 99,
        ...payload,
      };

      const dt = yield call(get, meetingroom.myapply, queryParams);
      const res = dt.data;
      if (!res) {
        console.log(dt);
        throw new Error('查询待审批数据失败, 远程请求可能不正确');
      }

      if (res.rowCount === 0) {
        yield put({ type: 'alert', payload: { title: '失败', message: '没有查询到数据!' } });
        return undefined;
      }

      const detail = res.results.filter(x => x.appointId === payload.appointId)[0];
      if (!detail) {
        yield put({ type: 'alert', payload: { title: '错误', message: '没有查询到数据!' } });
        return undefined;
      }

      yield put({ type: 'syncState', payload: { detail } });
    },

    *cancelApply({ payload }, { call, put }) {
      const dt = yield call(post, meetingroom.cancel, payload);
      const res = dt.data;
      if (!res) {
        console.log(dt);
        throw new Error('查询我的申请列表失败, 远程请求可能不正确');
      }

      if (!res.success && res.msg) {
        throw new Error(res.msg);
      }

      if (res.success) {
        yield put({ type: 'alert', payload: { title: '成功', message: '申请取消成功' } });
      }
    },

    *delApply({ payload }, { call, put }) {
      const dt = yield call(post, meetingroom.delApply, payload);
      const res = dt.data;
      if (!res) {
        console.log(dt);
        throw new Error('删除申请列表失败, 远程请求可能不正确');
      }

      if (!res.success && res.msg) {
        throw new Error(res.msg);
      }

      if (res.success) {
        yield put({ type: 'omitApply', payload });
        yield put({ type: 'alert', payload: { title: '成功', message: '删除申请成功' } });
      }
    },

    *approval({ payload }, { call, put }) {
      const dt = yield call(post, meetingroom.approval, payload);
      const res = dt.data;
      if (!res) {
        console.log(dt);
        throw new Error('查询我的申请列表失败, 远程请求可能不正确');
      }

      if (!res.success && res.msg) {
        throw new Error(res.msg);
      }

      if (res.success) {
        yield put({ type: 'alert', payload: { title: '成功', message: '审批成功' } });
      }
    },
  },

  reducers: {
    appendSchedules(state, { payload }) {
      const { weekdays, schedules, nextMonday, rooms } = payload;

      const newRooms = lodash.uniqBy([...state.rooms, ...rooms], x => x.roomCode);
      const newWeekdays = [...state.weekdays, ...weekdays];
      const newSchedules = { ...state.schedules, ...schedules };

      return {
        ...state,
        weekdays: newWeekdays,
        schedules: newSchedules,
        rooms: newRooms,
        nextMonday,
      };
    },

    updateRec(state, { payload }) {
      const { recId, status } = payload;
      const matchList = [];
      const noMatches = [];

      state.list.forEach((it) => {
        if (it.RecId === recId) {
          matchList.push({ ...it, Status: status });
        } else {
          noMatches.push(it);
        }
      });

      return { ...state, list: [...matchList, ...noMatches] };
    },

    syncDetail(state, { payload }) {
      let detail = (state.list || []).filter(x => x.appointId === payload.appointId)[0];
      if (!detail) {
        detail = (state.myApplies || []).filter(x => x.appointId === payload.appointId)[0];
      }
      if (!detail) {
        detail = (state.otherApplies || []).filter(x => x.appointId === payload.appointId)[0];
      }

      return { ...state, detail };
    },

    omitApply(state, { payload }) {
      const list = (state.list || []).filter(x => x.appointId !== payload.appointId);
      const myApplies = (state.myApplies || []).filter(x => x.appointId !== payload.appointId);
      const otherApplies = (state.otherApplies || []).filter(x => x.appointId !== payload.appointId);

      return { ...state, list, myApplies, otherApplies };
    },
  },
});
