import { Modal } from 'antd-mobile';

export default {
  namespace: 'base',

  state: {
    detail: {},
    list: [],
    newsType: [
      {
        value: '1',
        label: '公司新闻',
        page: 0,
      },
      {
        value: '2',
        label: '公 告',
        page: 0,
      },
      {
        value: '3',
        label: '部门动态',
        page: 0,
      },
      {
        value: '4',
        label: '集团新闻',
        page: 0,
      },
      {
        value: '5',
        label: '管理制度',
        page: 0,
      },
    ],
  },
  effects: {
  },

  reducers: {
    syncState(state, { payload }) {
      return { ...state, ...payload };
    },

    appendList(state, { payload }) {
      return { ...state, list: [...state.list, payload] };
    },

    alert(state, { payload }) {
      const { title, message, actions } = payload || {};
      Modal.alert(title, message, actions);
      return { ...state };
    },
  },

};
