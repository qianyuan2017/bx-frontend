
export default (app) => {
  app.model(require('./app'));
  app.model(require('./company/news'));
  app.model(require('./mine/mine'));
  app.model(require('./mine/salary'));
  app.model(require('./mine/timebook'));
  app.model(require('./meetingroom/optrecord'));
  app.model(require('./meetingroom/person'));
  app.model(require('./meetingroom/resource'));
};
