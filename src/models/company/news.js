import modelExtend from 'dva-model-extend';
import pathToRegexp from 'path-to-regexp';
import base from '../base';
import { get } from '../../utils/services';
import { company } from '../../utils/remote';
import route from '../../utils/routes';

export default modelExtend(base, {
  namespace: 'news',

  state: {},

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === route.company.news.list) {
          const regx = /type=([^?&#]+)/i;
          const m = regx.exec(location.search);
          const type = m ? m[1] : '1';
          dispatch({ type: 'listOnLoad', payload: { type, pageNo: 1, pageSize: 999 } });
          return undefined;
        }

        //
        // const m1 = pathToRegexp(`${route.company.news.detail}/:newsId`).exec(location.pathname);
        // if (m1) {
        //   dispatch({ type: 'detailOnLoad', payload: { newsId: m1[1] } });
        //   return undefined;
        // }
      });
    },
  },

  effects: {
    *listOnLoad({ payload }, { call, put, select }) {  // eslint-disable-line
      const { type, callback } = payload;
      const newsType = yield select(s => s.news.newsType);
      const { page } = newsType.filter(x => `${x.value}` === `${type}`)[0] || {};
      const pageNo = page + 1;
      const pageSize = 20;

      const dt = yield call(get, company.news.listOnLoad, { type, pageNo, pageSize });

      const res = dt.data;
      if (!res || res.length === 0) {
        // console.error(dt);
        if (callback) {
          callback();
        }

        // throw new Error('抱歉, 没有查询到数据!');
      }

      yield put({
        type: 'updateList',
        payload: { data: res, type },
      });

      if (res && res.length) {
        yield put({
          type: 'updatePageNo',
          payload: { page: pageNo, type },
        });
      }

      if (callback) {
        callback();
      }
    },
    *detailOnLoad({ payload }, { call, put }) {  // eslint-disable-line
      // const { newsId: appointId } = payload;
      // const dt = yield call(get, company.news.detailOnLoad, { appointId });
      // const res = dt.data;
      // if (!res || res.length === 0) {
      //   console.error(dt);
      //   throw new Error('抱歉, 没有查询到数据!');
      // }

      // yield put({
      //   type: 'syncData',
      //   payload: { detail: res[0] },
      // });
    },
  },

  reducers: {
    updateList(state, { payload }) {
      const { data: dts, type } = payload;
      const eqTypeList = [];
      const neqTypeList = [];

      (state.list || []).forEach((item) => {
        const { data = [] } = item;
        if (`${item.type}` === `${type}`) {
          eqTypeList.push({ type, data: [...data, ...dts] });
        } else {
          neqTypeList.push(item);
        }
      });

      if (!eqTypeList.length) {
        eqTypeList.push(payload);
      }

      return { ...state, list: [...eqTypeList, ...neqTypeList] };
    },

    updatePageNo(state, { payload }) {
      const originType = state.newsType;
      const { page, type } = payload;
      const newsType = originType.map((item) => {
        return `${item.value}` === `${type}`
          ? { ...item, page }
          : item;
      });

      return { ...state, newsType };
    },
  },
});
